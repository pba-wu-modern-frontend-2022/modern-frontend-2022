![Build Status](https://gitlab.com/ucl-pba-wu/modern-frontend-2022/badges/main/pipeline.svg)

# modern frontend 2022

Projekt til undervisnings materiale for modern frontend faget i foråret 2022

# Website

- [https://ucl-pba-wu.gitlab.io/modern-frontend-2022](https://ucl-pba-wu.gitlab.io/modern-frontend-2022)
