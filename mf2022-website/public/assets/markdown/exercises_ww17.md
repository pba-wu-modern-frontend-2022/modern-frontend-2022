---
name: Opgaver uge 17
week: 17
tags:
  - VueJS
  - Event Bus
  - Komponent Kommunikation
  - Filters
  - State Management
---

# Opgaver uge 17

Opstart med ny underviser, lad os diskutere:

- rækkefølgen på den sidste undervisning, kan vi optimere den for at I får mest ud af undervisningen inden eksamen?
- **backend test environment** undervisningsgangen; er det noget der skal rettes til så det er tilpasset jeres behov?
  - Strapi
  - Node Server
  - (wordpress.com)
- Status på eksamensprojekt

I har tidligere arbejdet med kommunikation mellem komponenter, ved brug af **props** og **$emit**. Dette er et fantastisk værktøj, men bliver en smule begrænset i større projekter. Derfor skal I, i denne uge, arbejde med VueJS main bus.
Ydermere skal I arbejde med Vue's state manager. Der skal I prøve 2 udgaver af. Både det helt nye Pinia og den eksisterende Vuex. Det er til tider nødvendigt at kigge på de ældre udgaver af software, da produktionsmiljøer ikke altid bevæger sig lige så hurtigt som man kunne ønske sig.
Sidst men ikke mindst skal I denne gang også kigge på "filtre", der I Vue 3 er meget anderledes og på mange måder simplificeret ift. til Vue 2 og tidligere.

## Opgave 1 - Udvidet kommunikation mellem komponenter

_team: 90 minutter_

### Information

I følgende opgave skal I forstå brugen af event bus. I skal bl.a. vurdere og forstå fordele og ulemper ved brugen af en event bus.

Event bus er nødvendig funktionalitet i større web applikationer og kan spare mange udvikler ressourcer, men har ligeledes også nogle udfordringer. Dem skal I vurdere.
I den forbindelse må I gerne vurdere encapsulation og reusability osv.

### Instruktioner

1. Læs følgende artikel [Using Event Bus in Vue.js 3](https://medium.com/@certosinolab/using-event-bus-in-vue-js-3-425aae8c21a6)
2. Opdater jeres eksisterende component diagram, til at understøtte brugen af event bus component communication
3. Diskuter internt i jeres teams hvor fordelene og ulemperne ved de 2 måder at kommunikationen mellem komponenter er (husk at tage note af det. Det er godt til eksamen)
4. Implementer nu event bus på de steder hvor det giver mening at implementere i jeres web app.

## Opgave 2 - Udvidet kommunikation mellem komponenter

_team: 15 minutter_

### Information

Den regulære metode, I allerede har lært med props/$emit, er fantastisk hvis der primært skal kommunikeres mellem parent child components. Denne approach er dog sjældent nok, efterhånden som softwaren udvider sig, hvor components begynder at sprede sig ud horisontalt og kan derfor have svært ved at kommunikere med hinanden.

Denne opgaves fokus er en alternativ approach til at implementere event bus. Husk på at denne ikke kan implementeres direkte i Vue 3

### Instruktioner

1. [COMPONENT COMMUNICATION (PROPS / EVENTS) | VueJS | Learning the Basics](https://www.youtube.com/watch?v=PPmg7ntQjzc) (OPTIONAL, det har i fået introduceret tidligere)
2. [Transfer Data With Event Bus In Vue JS](https://www.youtube.com/watch?v=vBks1Naa8Tk)
3. Implementer denne løsning som alternativ til approachen i Opgave 1, hvis det giver bedre mening for jeres projekt.

## Opgave 3 - Filters

_Individuel: 40 - 60 min_

### Information

En grundlæggende funktionalitet JavaScript er brugt til, er filtrering af data. Det er nødvendigt ved søgning, lister etc. Det skal I selvfølgelig også have forståelse for.
Der har været et stort skift fra Vue 2 til Vue 3 og I kan derfor med fordel vælge den platform I primært bruger, men det er altid en god ide at som minimum forstå begge approaches til filters.

### Instruktioner

1. Læsning, du bør læse begge appraches
  * [Vue.js Filters](https://flaviocopes.com/vue-filters/) Filters for Vue 2
  * [Create a Search Filter Aop with Vue 3 and JavaScript](https://thewebdev.info/2021/01/14/create-a-search-filter-app-with-vue-3-and-javascript/) Filters for Vue 3
2. Med udgangspunkt i den Vue version I arbejder i, brug nu tid på at bygge et filter der passer ind I jeres software.

## Opgave 4 - State Management

_Team: 90 - 120 min_

### Information

State management den grundlæggende måde at centralisere data gemt i applikationen man arbejder med. I Vuejs har man skiftet fra Vuex til Pinia, hvilket som med alt andet der ændrer sig. Betyder at der vil være et overlap, hvor begge løsninger bliver præsenteret i materialet på nettet.
Det er derfor fornuftigt at som minimum haave en basal forståelse for begge state managers. I jeres opgave vælger I selv hvilken state manager I vil bruge, dog vil jeg foreslå at I har læst blog indlægget i punkt 3 under instruktioner, før valget tages.

### Instruktioner

1. Grundlæggende teori [State Management](https://v2.vuejs.org/v2/guide/state-management.html)
2. Information om de 2 primære State managers
  * [Vuex](https://vuex.vuejs.org/)
  * [Pinia](https://pinia.vuejs.org/introduction.html)
3. [Pinia vs. Vuex: Which state management library is best for Vue?](https://blog.logrocket.com/pinia-vs-vuex/)
4. Baseret på jeres valg, opbyg nu en centraliseret logik for jeres applikation, så I gør brug af state manager
  * Test at I kan få en state manager op at køre og at data hvor det giver mening er stored
  * I alle situationenr i jeres software, skal I vurdere hvorvidt det er bedst at have state manager, event bus osv.

