# WUOE21 Modern frontend development

## Deploy prototype aflevering

Afleveringen er det slideck som i brugte til at præsentere jeres deploy protoype i uge 12.

Indholdet af slidedecket skal være:

- Kort om ideen bag applikationen (kontekst)
- Præsentation af applikationens arkitektur (api, hosting osv.)
- Præsentation af applikationens opbygning (komponent diagram, brug af fetch, sass osv.)
- Team navn, navne på team medlemmer og link til prototype skal inkluderes i slidedecket.

Aflever venligst slidedecket som en `.pdf` fil

## Kontakt

Ved tvivl kontakt Nikolaj Simonsen på mail nisi@ucl.dk
