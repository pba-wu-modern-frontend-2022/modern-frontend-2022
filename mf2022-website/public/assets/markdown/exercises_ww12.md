---
name: Opgaver uge 12
week: 12
tags:
  - deploy prototype
  - forberedelse
---

# Opgaver uge 12

## Opgave 1 - Forberedelse til præsentation af deploy prototype

_team: hele dagen_

### Information

I næste uge skal i præsentere jeres arbejde i modern frontend development, krav til præsentationen samt tidsplan finder i under næste uges opgaver.

I har hele dagen i uge 12 til, i jeres team, at udfærdige og planlægge materiale til præsentationen.
Deltagelse i præsentationen er en obligatorisk forudsætning for at deltage i eksamen for modern frontend development.

### Instruktioner

1. Dan jer et overblik over hvad i skal nå for at være klar til at præsentere i næste uge.
2. Opret tasks/issues der sikrer at i får udført arbejdet.
3. Fordel tasks/issues
4. Arbejd på aftalte tasks/issues
5. Øv jeres præsentation igennem så i er sikre på at alle ved hvad de skal gøre og sige indenfor tidsrammen (15 minutter)
