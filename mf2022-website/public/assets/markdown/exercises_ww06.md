---
name: Opgaver uge 6
week: 6
tags:
  - Frontend frameworks
  - VueJS introduktion
  - Vue ressourcer
  - Projektbeskrivelse
---

# Opgaver uge 6

I denne uge går vi igang med Vue, i skal have et overblik over hvordan Vue virker, hvor man kan finde dokumentation osv.  
Ud over forståelse, skal i bruge overblikket til at installere Vue og Vue cli.

Jeg har valgt et rigtig godt og gratis video kursus lavet af The Net Ninja.  
Udover kursets kvalitet er grunden til at jeg har valgt det at i får mulighed for at tilegne jer viden og kompetencer i jeres eget tempo, i kan altid gå tilbage i materialet og få ting gentaget, i har mulighed for at følge med hvis i, eller jeg, bliver syge og endelig afspejler det fint en måde at tilegne sig ny viden på egen hånd, noget i har brug for efter endt studie.

Når i begynder at føle jer hjemme i vue, vil i sandsynligvis fortrække at bruge den officielle dokumentation samt google søgninger på specifik viden og metoder. Det er hurtigere at bruge skreven dokumentation når i skal bruge specifik viden.  
Herunder er links til både kursets playliste samt vue3's officielle dokumentation.

Udover det tekniske skal hvert team have besluttet hvilket projekt i vil bygge igennem semestret, det skal selvfølgelig bygges i Vue.  
Projektet skal beskrives i en projektplan der i dag skal udarbejdes i en første version som i kan bruge til at starte med at arbejde i næste uge.

Husk at det ikke handler om at udføre opgaverne hurtigst muligt, det handler om at forstå mest muligt. Så mens du udfører opgaverne kan det være nødvendigt at undersøge de ting du er nysgerring på, eller undrer dig over, nærmere. Det kan du gøre ved at søge efter yderligere materiale, eller stoppe op og snakke med dit team, om hvordan de forstår en ting eller et emne.  
Jo mere du arbejder i dybden, jo mere vil du forstå og huske til senere brug, når du arbejder i dybden bliver det rigtig sjovt at lære!

_Links:_

- vue3 documentation [https://v3.vuejs.org/](https://v3.vuejs.org/)
- Net Ninja - Vue JS 3 Tutorial for Beginners playliste [https://youtube.com/playlist?list=PL4cUxeGkcC9hYYGbV60Vq3IXYNfDk8At1](https://youtube.com/playlist?list=PL4cUxeGkcC9hYYGbV60Vq3IXYNfDk8At1)

## Opgave 1 - Javascript frameworks viden

_team opgave: 60 minutter_

### Information

I denne opgave skal i danne jer et overblik over begrebet frontend frameworks, hvorfor findes de? Hvornår skal i benytte et og hvilket skal i bruge til en given opgave ?

Javascript frameworks er en essentiel del af moderne frontend udvikling. javascript frameworks er en samling af værktøjer som er veldokumenterede og afprøvet, det indkapsler alle de dele som en typisk web applikation består af. På den måde kan du som udvikler koncentrere dig om at udvikle det essentielle i en given web applikation. Javascript frameworks gør det forholdsvist (i forhold til at lave samme funktionalitet fra bunden) let at bygge skalerbare og interaktive web applikationer.

### Instruktioner

1. _20 minutter_ Læs [https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Introduction](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Introduction)

   - Mens du læser, stil dig selv følgende spørgsmål og noter dine svar.
     - Hvilke kompetencer skal du have for at arbejde med et frontende framework ?
     - Hvad er forskellen på statiske og dynamiske web applikationer ?
     - Hvorfor er der et behov for frontend frameworks/Hvad er fordelen ?
     - Hvad er forskellen på server-side routing og client-side routing ?
     - Sikrer frontend frameworks dig kvalitet i din web applikation ?
     - Giv nogle bud på hvad en web udviklings virksomhed bør være opmærksomme på når de skal vælge et frontend framework.

2. _10 minutter_ I jeres team tag en bordet rundt hvor i præsenterer og diskuterer hinandens svar, målet er at i som team bliver enige om jeres svar. Noter teamets svar.
3. _30 minutter_ Som afslutning på denne opgave skal i som team præsentere jeres svar på klassen.  
   Svarene bruger vi som oplæg til en debat, hvor andre teams kan tilføje og stille spørgsmål.  
   Et svar pr. team, hele teamet præsenterer samlet og bliver ved tavlen når der stilles spørgsmål.

## Opgave 2 - Vue overblik

_individuel opgave: 120 - 180 minutter_

### Information

I denne første vue opgave er målet at få et overblik over hvordan vue fungerer og hvordan vue kan bruges fra et Content Delivery Network ([What is a CDN](https://www.cloudflare.com/learning/cdn/what-is-a-cdn/))  
Jeg har fundet et rigtig godt video kursus som er gratis, meningen er at i skal følge det og gøre de samme ting som der gøres i kurset. Det er altså ikke nok at se videoerne. Hvis du kun ser videoerne lærer du ikke at gøre det i virkeligheden hvilket er det vigtigeste.

Ud over videoerne vil jeg gerne have at i bruger den officielle vue3 dokumentation som kan findes på - vue3 documentation [https://v3.vuejs.org/](https://v3.vuejs.org/)

Husk at skrive eventuelle spørgsmål ned som du kan spørge din underviser eller medstuderende om.

Den kode som du skriver mens du udfører opgaven skal gemmes i en mappe i dit teams github/gitlab. Navngiv mappen fornuftigt.

### Instruktioner

1. Det første du skal er at se og udføre det samme som vises i video 1. Det vil give dig et overblik over vue og hvad mulighederne er.  
   _Net Ninja - Vue JS 3 Tutorial for Beginners #1 - Introduction_ [https://youtu.be/YrxBCBibVo0](https://youtu.be/YrxBCBibVo0)
2. Video 2 viser hvordan du bruger vue fra et cdn, hvad vue app er, hvad data & templates betyder, hvordan vue håndterer click events og hvad conditional rendering er. Se videoen og udfør det der bliver vist mens du ser videoen.  
   _Net Ninja - Vue JS 3 Tutorial for Beginners #2 - Vue.js Basics (part 1)_ [https://youtu.be/F7PLPJqVotk](https://youtu.be/F7PLPJqVotk)
3. Den tredje vue introduktionsvideo giver dig et overblik over andre typer mouse events (mouse over etc.), list rendering med v-for, attribute binding, dynamic css classes og computed properties  
   Se og udfør _Net Ninja - Vue JS 3 Tutorial for Beginners #3 - Vue.js Basics (part 2)_ [https://youtu.be/CYPZBK8zUik](https://youtu.be/CYPZBK8zUik)

## Opgave 3 - Projektplan

_team opgave: 240 minutter (måske mere)_

### Information

Igennem hele semestret skal i arbejde på et frontend projekt. Projektet danner grundlag for jeres eksamens rapport i slutningen af semestret.

Vi, Kenneth og jeg, har diskuteret om projektet skal være en bundet opgave som er ens for alle teams eller om i skal bestemme hvilket projekt i vil lave. Vi er blevet enige om at i selv skal have lov til at vælge hvilket projekt i vil lave fordi vi mener at det vil give jer den bedste motivation og ejerskab.

Til gengæld stiller det store krav til jeres planlægning og projekt beskrivelse fordi applikationen selvfølgelig skal leve op til de faste projekt krav som er beskrevet på itslearning i fagplanen for Modern Frontend Development.  
Udover det skal i lave et projekt som gør jer i stand til at vise, i rapporten samt til eksamen, at i har opnået fagets læringsmål.

Det er vigtigt at i har besluttet i jeres team hvilken applikation i vil lave igennem semestret, senest i næste uge, derfor har jeg sat hele eftermiddagen af til at i kan brainstorme og ideudvikle.  
I bør ikke undervurdere at denne opgave tager lang tid og at jeres beslutninger er vgtige for hele projektet.  
I uge 7 skal projektplanen være udfyldt og afleveret til godkendelse, seneste den 16. februar. Det er undervisere der godkender planen.  
Projektplanerne vil i få feedback på i uge 8.

Projektplanen er jeres styringsdokument for projektet, det er helt normalt at projektplanen tilrettes gennem projektet, et projekt er trods alt defineret ved at det er noget som ikke er prøvet før.

### Instruktioner

1. Lav en åben brainstorm hvor hele teamet bidrager til hvad projektet og applikationen skal indeholde. Alle ideer er gyldige uanset hvor vilde de er!
2. Dan jer et overblik over hvor meget tid i har til rådighed til at bygge applikationen, i kan regne med at i kan arbejde hver torsdag eftermiddag, samt hele uge 9. Afleverings dato kan ses på itslearning i rummet **Eksamener 2022**
3. Fra jeres brainstorm finder i de 2 bedste og mest realistiske/brugbare ideer. Hvem er kunden, hvem er målgruppen, hvem kan have nytte af projektet osv.
4. Find en projektstyrings metode i vil anvende, jeg vil anbefale noget scrum agtigt [https://www.scrum.org/resources/what-is-scrum](https://www.scrum.org/resources/what-is-scrum) Det vigtige er at i aftaler nogle faste møder hvor i opdaterer hinanden på projektets status og fordeler opgaver så alle har noget at arbejde med hele tiden. Det er også nødvendigt at aftale hvordan i vil samarbejde om kode med GIT. Her kan i for eksempel tage udgangs punkt i en branching strategi [https://docs.gitlab.com/ee/topics/gitlab_flow.html](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
5. Lav en risiko vurdering der indeholder en beskrivelse af 10 ting der kan gå galt i projektet og hvad i vil gøre hvis hver enkelt ting går galt.
6. Lav en team kontrakt hvor i beskriver jeres belbin roller inkl. styrker og svagheder, hvordan i kan kontakte hinanden, hvilken arbejds indsats i kan forvente af hinanden osv. Formålet er at i får afstemt forventninger til hinanden og hvilke krav derer realistiske at forvente af jeres team medlemmer.
7. Lav en tidsplan med milestones for projektet, det skal bare være de store blokke i projektet, ikke mange detaljer.
8. Udfyld en projektplan som indeholder:
   - Team navn
   - Deltagere
   - En kort beskrivelse af ideerne til applikationen, beskriv begge 2, den ene er plan a og den anden plan b.
   - Valgt projektstyrings metode
   - Risikovurdering
   - Team kontrakt
   - Tidsplan med milestones
   - En mere grundig beskrivelse af projektet og ideerne. Den skal indeholde en beskrivelse af hvordan applikationen kan leve op til de tekniske og funktionelle krav, samt hvordan projektet lever op til fagets læringsmål. Her vil vi gerne have at i besvarer hvert enkelt punkt i hhv. tekniske og funktionelle krav samt hvert enkelt læringsmål.
9. Læg projektplanen i jeres github/gitlab projekt som en .md fil og link til filen i team dokumentet [https://docs.google.com/document/d/1M2JEnv71nqK5GroJmJvUCmHT1RfGJXCLmoJRaydzTIQ/edit?usp=sharing](https://docs.google.com/document/d/1M2JEnv71nqK5GroJmJvUCmHT1RfGJXCLmoJRaydzTIQ/edit?usp=sharing)
