---
name: Opgaver uge 10
week: 10
tags:
  - Forms
  - Input
  - 2-way data binding
  - input validering
  - JS array filter
  - Vue router
  - Router Links
  - Route parameters
  - Dynamic links
  - Redirects
---

# Opgaver uge 10

Efter en hel uge med projekt arbejde håber vi at i er kommet godt i gang med jeres planlagte vue projekt.  
Denne uge er planlagt til at have en 1/2 dag hvor i arbejder videre i teamet med jeres projekt (opgave 3), den anden halvdel af dagen er planlagt til at i fortsætter individuelt med at lære mere om vue (opgave 1 og 2)

Når i arbejder individuelt med vue videoerne fra net ninja så kan i med fordel checke hans kode eksempler på github. Hver af hans videoer indeholder x antal lektioner. Hver lektion er en seperat git branch og på den måde kan du se den specifikke kode for hver lektion.  
Jeg har gjort det at jeg har klonet hans github repository til min computer (selvfølgelig med ssh) og på den måde kan jeg afpræve hans løsninger lokalt.

Jeg skifter mellem hans branches med git kommandoen `git checkout <branchname>` som eksempel vil `git checkout lesson-38` skifte til koden for lektion 38.  
Du skal huske at installere node modules med npm kommandoen `npm install` eller `npm i` før du kan køre løsningen med `npm run serve`

Link til net ninja github repo er her: [https://github.com/iamshaunjp/Vue-3-Firebase/tree/master](https://github.com/iamshaunjp/Vue-3-Firebase/tree/master)

## Opgave 1 - Vue Forms, input og validering

_individuelt: 120 minutter_

### Information

Et af de funktionelle krav til jeres vue aplikation er:  
_Applikationen skal have en eller flere input forms og der skal som minimum implementeres validering in front-end, men gerne suppleres med back-end validering._  
Derfor har i brug for at vide hvordan man håndterer forms og user input og hvordan i kan lave input validering (frontend) i vue.  
Øvelsen vil også vise hvordan 2-way data binding fungerer i vue samt vise hvordan javascript filter array metoden fungerer.

Den officielle dokumentation for forms i vue er her: [https://vuejs.org/guide/essentials/forms.html#select](https://vuejs.org/guide/essentials/forms.html#select)
Javascript array filter er dokumenteret her: [https://www.w3schools.com/jsref/jsref_filter.asp](https://www.w3schools.com/jsref/jsref_filter.asp)

### Instruktioner

1. Lav en ny vue applikation eller byg videre på en af dem du har lavet i tidligere uger.
2. Se og udfør det samme som vises i video 7 (lesson 38 - 44). Det vil give dig et overblik over forms i vue.  
   _Net Ninja - Vue JS 3 Tutorial for Beginners #7 - Forms & Inputs_ [https://youtu.be/ixOcve5PX-Q](https://youtu.be/ixOcve5PX-Q)
3. commit/push dit arbejde til gitlab/github mens du arbejder. Husk at skrive rigeligt med kommentarer i din kode så du kan bruge hvad du har lært senere.

## Opgave 2 - Vue router

_individuelt: 120 minutter_

### Information

Et krav til jeres projekt lyder _Routing skal implementeres med vue router_ og denne øvelse handler om netop vue-router.  
vue-router er en npm pakke som installeres ovenpå et eksisterende vue projekt eller ved hvjælp af vue-cli når et nyt vue projekt initieres med `vue create <projektnavn>`

Routing bliver aktuelt når en hel side skal udskiftes, f.eks med en about side eller anden underside. Ved konventionelle web applikationer vil routing mellem undersider resultere i hentning af ny data fra serveren, det tager tid og skaber netværkstrafik.
Som bekendt er en SPA defineret ved at web applikationen kun sender en forespørgsel til serveren, herefter håndteres logik i klientens browser ved brug af javascript.  
Routing mellem forskellige views er dog stadig muligt ved hjælp af vue router. Vue router håndterer logikken som muliggør routing og browserens history (frem og tilbage knapperne)
Fordi der ikke sendes yderligere forespørgsler til serveren er routing mellem forskellige views i en SPA meget hurtige.

I denne øvelse skal du arbejde med vue-router for at blive bekendt med funktionalitet og syntax, målet er at kunne implementere det i jeres fælles projekt.

- Vue router dokumentation er her: [https://router.vuejs.org/](https://router.vuejs.org/)
- Vær opmærksom på at vue-router har 2 forskellige history modes, hvilken du vælger afhænger af serverens opsætning. Hvis du ikke har adgang til at konfigurere din server så anbefales det at vælge **Hash Mode** fremfor **HTML5 Mode** [https://router.vuejs.org/guide/essentials/history-mode.html#internet-information-services-iis](https://router.vuejs.org/guide/essentials/history-mode.html#internet-information-services-iis)

Som bonus giver the net ninja også forslag til folder structure i video 8 :-)

### Instruktioner

1. Lav en ny vue applikation eller byg videre på en af dem du har lavet i tidligere uger.
2. Se og udfør det samme som vises i video 8 (lesson 46 - 53).  
   _Net Ninja - Vue JS 3 Tutorial for Beginners #8 - The Vue Router_ [https://youtu.be/juocv4AtrHo](https://youtu.be/juocv4AtrHo)
3. commit/push dit arbejde til gitlab/github mens du arbejder. Husk at skrive rigeligt med kommentarer i din kode så du kan bruge hvad du har lært senere.

## Opgave 3 - Projekt arbejde

_team: 240 minutter_

### Information

Den her opgave er primært for at i husker at arbejde videre med jeres vue projekt. Samtidig er det en måde at gøre er opmærksomme på at i skal arbejde hen i mod at vise en _deploy prototype_ på klassen den 31. marts.  
Med _deploy prototype_ mener vi en prototype af jeres vue applikation hostet offentligt. Prototypen behøver ikke at indeholde meget funktionalitet men dog nok til at vi alle kan få en grundlæggende ide om hvad det er for an løsning i er i gang med at udvikle.  
Det vil sige at en udgivelse af vue boilerplate projektet ikke er nok.  
Præsentationen er en obligatorisk aktivitet som alle skal deltage i for at kunne deltage i eksamen, det vil sige at alle skal deltage i præsentationen for at få adgang til eksamen. Der vil også være en aflevering af jeres præsentation på wiseflow.

I får en hel dag den 24. Marts til at forberede præsentationen.

### Instruktioner

1. Dan jer et overblik over hvor i er henne i projektet og hvad målet for dagens arbejde er.
2. Snak i teamet om hvad i gerne vil kunne vise til præsentationen den 31. Marts og lav en milestone i jeres projekt på gitlab/github
3. Opret tasks/issues der sikrer at i får planlagt implementering af forms, input validering og routing (brug jeres projektplan her)
4. Fordel tasks/issues
5. Arbejd på aftalte tasks/issues
