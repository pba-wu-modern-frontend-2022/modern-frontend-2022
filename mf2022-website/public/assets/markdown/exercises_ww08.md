---
name: Opgaver uge 8
week: 8
tags:
  - opsætning
  - toolchain
  - projekt arbejde
  - planlægning
---

# Opgaver uge 8

## Opgave 1 - Vue projekt toolchain opsætning

_team: 30-60 minutter_

### Information

Denne opgave handler om at i som team får opsat vue så det kan bruges gennem resten af semestret til jeres projekt.  
Det viste er foreslag og i er velkomne til at afprøve yderligere features i vue-cli.  
I skal i teamet samarbejde, men selvfølgelig kun konfigurere et projekt som øvrige team medlemmer efterfølgende puller til deres computer.

Hvis noget driller kan i kigge i dette boilerplate projekt jeg har lavet  
[https://gitlab.com/pba-wu-modern-frontend-2022/modern-frontend-2022/-/tree/main/exercise_projects/sass-config](https://gitlab.com/pba-wu-modern-frontend-2022/modern-frontend-2022/-/tree/main/exercise_projects/sass-config)

### Instruktioner

1. Nyeste vue cli version er 5.0.1, hvis du har en lavere vesion så opdater med `npm i -g @vue/cli`
2. Lav et nyt vue projekt og navngiv det så det har den rigtige titel i forhold til jeres projekt `vue create <projektnavn>`
3. Vælg features som vist herunder:

   <img src="assets/images/vue_cli_selected_features.png" alt="vue cli selected features overview" />

4. Når projektet er oprettet så kør det for at bekræfte at det virker `npm run serve`

**Ret default filer**

5. Erstat indholdet i `HelloWorld.vue` med

   ```html
   <template>
     <h1 class="text-large">Template is clean</h1>
   </template>

   <script>
     export default {};
   </script>

   <style></style>
   ```

6. Ret `App.vue` til

   ```html
   <template>
     <HelloWorld />
   </template>

   <script>
     import HelloWorld from './components/HelloWorld.vue';

     export default {
       name: 'App',
       components: {
         HelloWorld,
       },
     };
   </script>

   <style lang="scss"></style>
   ```

**konfigurer projekt til sass**

7. Lav en mappe der hedder `scss` i `src` mappen

   ```
   src
   ├───assets
   │ └───scss
   └───components
   ```

8. I `scss` mappen lav 2 filer, `app.scss` og `_hello-world.scss`

   ```
   ├───assets
   │ │ logo.png
   │ │
   │ └───scss
   │        app.scss
   │        _hello-world.scss
   │
   └───components
            HelloWorld.vue
   ```

9. `app.scss` er den fil i kan bruge til at importere sass partials. I filen, importér `_reset.scss` ved at skrive

   ```scss
   @use '_hello-world.scss';
   ```

10. For at vue ved at du gerne vil bruge `app.scss` til styling skal du i `App.vue` i styling tagget nederst angive at du bruger filen.

    ```html
    <style lang="scss">
      @import './assets/scss/app.scss';
    </style>
    ```

11. Lav noget styling på `text-large` klassen i `_hello-world.scss` for at se at sass compileren virker, f.eks.
    ```scss
    .text-large {
      font-size: 2em;
      color: deeppink;
    }
    ```
12. Lav en .gitignore fil til `node_modules` mappen
13. Add, commit og push til gitlab/github
14. Øvrige team medlemmer puller projektet til deres lokale computer, installerer node moduler med `npm install` og bekræfter at det kan køres med `npm run serve`
15. Snak i teamet om hvordan i strukturerer jeres `sass` komponenter, skal i have variabler i en fil?, skal der være en fil pr. komponent? etc.
16. Dokumenter jeres sass struktur i en `.md` fil i jeres gitlab/github projekt

## Opgave 2 - komponent diagram

_team: 30-60 minutter_

### Information

Som vi snakkede om i sidste uge er det en god ide at have et diagram der viser de forskellige komponenter i har i jeres vue projekt.  
At lave et komponent diagram er også et godt planlægnings værktøj, her kan i begynde som team at planlægge hvordan applikationens komponenter skal struktureres og hvordan data skal udveksles mellem komponenter som hhv. `props` og `$emit`

I kan bruge et hvilket som helst tegneprogram, jeg plejer at bruge [https://www.diagrams.net/](https://www.diagrams.net/)

### Instruktioner

1. Læs om at lave vue component diagrams [https://v2.vuejs.org/v2/guide/components.html?redirect=true#Organizing-Components](https://v2.vuejs.org/v2/guide/components.html?redirect=true#Organizing-Components)
2. Brainstorm ud fra jeres projektplan om hvilke komponenter jeres applikation skal have og hvordan de skal organiseres. Angiv på komponentdiagrammet eventuel kommunikation mellem komponenter med enten `props` og/eller `$emit`
3. Husk at linke/inkludere diagrammet i jeres dokumentation på gitlab/github

## Opgave 3 - Uge 9 planlægning

_team: 60 minutter_

### Information

Uge 9 er en hel uge hvor i kan arbejde på jeres projekt, for at i får så meget ud af ugen skal i planlægge opgaver for ugen.  
Sørg for at planlægge mere end i kan nå, så er i sikre på at i ikke løber tør for arbejde.

### Instruktioner

1. Lav opgaver til `37 timer * antal team medlemmer` på jeres kanban board, husk s.m.a.r.t akronymet når i laver opgaver  
   _PS. det er 148 timers arbejde for et team på 4 medlemmer!! så i burde kunne nå at komme rigtig godt ind i projektet hvis i planlægger godt._
2. Sorter/prioritér opgaverne
3. Assign en opgave til hvert team medlem så i ved hvad i skal starte med at lave mandag morgen.
