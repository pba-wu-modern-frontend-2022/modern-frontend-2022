---
name: Opgaver uge 13
week: 13
tags:
  - deploy prototype
  - præsentation
  - composition api
---

# Opgaver uge 13

## Opgave 1 - præsentation af deploy prototype

_team: 15 minutter_

### Information

I denne uge skal i præsentere jeres deploy prototype. Med deploy prototype menes en prototype der er udgivet så den er offentligt tilgængelig (hostet på en offentlig server)

Hvert team har 15 minutter til præsentation og det forventes at alle i teamet deltager aktivt, det vil sige at alle siger og gør noget i løbet af præsentationen.  
Præsentationen skal bestå af et slidedeck der efterfølgende afleveres på wiseflow som en gruppe aflevering i `.pdf` format.  
I får en invitation til wiseflow afleveringen på jeres studie mail.

Deltagelse i præsentationen er en obligatorisk forudsætning for at deltage i eksamen for modern frontend development.

### Instruktioner

**Indhold af præsentation:**

1. Slidedeck

   - Kort om ideen bag applikationen (kontekst)
   - Præsentation af applikationens arkitektur (api, hosting osv.)
   - Præsentation af applikationens opbygning (komponent diagram, brug af fetch, sass osv.)
   - Team navn, navne på team medlemmer og link til prototype skal inkluderes i slidedecket.

2. Live demo:

   - Demo af protoype funktionalitet

**Tidsplan**

| team   | tid           |
| :----- | :------------ |
| team 1 | 9:00 - 9:15   |
| team 2 | 9:20 - 9:35   |
| team 3 | 9:40 - 9:55   |
| pause  | 10:00 - 10:10 |
| team 4 | 10:15 - 10:30 |
| team 5 | 10:35 - 10:50 |
| team 6 | 10:55 - 11:10 |
| team 7 | 11:15 - 11:30 |
