---
title: Opgaver uge 5
week: 5
tags:
  - Style processing
  - Naming conventions (BEM)
  - CSS Preprocessors (sass)
  - HTML semantics
  - Node
  - NPM
---

# Opgaver uge 5

Opgaverne i denne uge er en god blanding af html, sass og en masse command line (cli).

Personligt bruger jeg den indbyggede terminal i VS code når jeg skal bruge cli mens jeg koder.  
Du kan sagtens bruge din normale terminal men du skal være opmærksom på at du eksekverer kommandoer i de rigtige mapper.

I skal sidde sammen med jeres team og lave opgaverne, også de opgaver som er individuelle.  
Grunden til at i skal sidde sammen er så i kan hjælpe hinanden hvis noget er svært at forstå.

Husk at opgaverne er til for at facilitere din læring og du skal kun lave dem for at lære, ikke for underviserens skyld :-)  
I må meget gerne gå ud over øvelserne og prøve andre ting af hvis du bliver nysgerrig.

## Opgave 1 - installer node.js

_individuel opgave: 45 minutter_

### Information

Node.js er, som vi snakkede om i sidste uge, chrome's V8 javascript engine. Node.js kan bruges til mange ting.  
Se for eksempel denne artikel [https://www.netguru.com/blog/node-js-apps](https://www.netguru.com/blog/node-js-apps) og s'g selv efter flere ting som Node.js kan bruges til.
En af de ting der følger med Node.js er Node Package Manager (NPM) som er tilføjelser/biblioteker til Node.js der kan downloades og bruges i dine egne projekter.  
Når vi starter med at bruge Vue skal vi blandt andet bruge NPM.  
Til dagens opgaver skal vi bruge både Node.js og NPM, så derfor er første opgave at få det installeret.

_**Fun fact:**_ Stifteren af Node.js, Ryan Dahl, forlod for et par år siden Node.js projektet for at starte et nyt og ifølge ham bedre projekt baseret på V8 javascript motoren, projektet kan findes her [https://deno.land/](https://deno.land/)
På wikipedia kan du finde lidt historie om deno og Ryan Dahl [https://en.wikipedia.org/wiki/Deno\_(software)](<https://en.wikipedia.org/wiki/Deno_(software)>)

### Instruktioner

1. Download fra [https://nodejs.org](https://nodejs.org)
2. Installer Node.js, hvis du er i tvivl om hvordan er her en guide [https://www.geeksforgeeks.org/installation-of-node-js-on-windows/](https://www.geeksforgeeks.org/installation-of-node-js-on-windows/)
3. Konfirmér at node.js er installeret ved at skrive `node -v` i en terminal. Hvis du får et versionsnummer i dit output er alt ok, hvis ikke så start forfra på opgaven.
4. Konfirmér at npm (node package manager) er installeret ved at skrive `npm -v` i en terminal. Hvis du får et versionsnummer i dit output er alt ok, hvis ikke så start forfra på opgaven.
5. Lav en ny mappe på din computer, åbn en terminal og naviger til mappen.
6. Nu skal du lave et nyt node.js projekt. I din terminal skriv `npm init` og derefter følge instruktionerne i terminalen.
7. Åbn mappen i din kode editor (VS code, sublime etc.) og kontroller at du har en fil der hedder `package.json`

## Opgave 2 - installer live server

_individuel opgave: 30 minutter_

### Information

Måske kender du allerede live server?  
Hvis ikke kommer her en kort forklaring, live-server er en npm pakke der installerer en development server som kan vise preview af dit arbejde mens du udvikler.  
En af de gode ting ved live-server er at den automatisk opdaterer din browser når du gemmer ændringer i dine html/css/js filer.

Live server skal installeres globalt med `-g` parametret for at virke.

### Instruktioner

1. Nu skal du installere din første npm pakke som er `live server` med kommandoen `npm install -g live-server`
2. Opdater `package.json` script afsnittet til:
   ```json
   "scripts": {
       "live-server": "live-server"
     }
   ```
3. Lav to nye filer i din projekt mappe fra opgave 1, `index.html` og `style.css`
4. Skriv lidt html i `index.html`, bare en H1 eller noget
5. Link til `style.css` fra `index.html`
6. Lav lidt styling på H1 tagget fra `style.css`
7. Start live-server fra din terminal med `npm run live-server`. Hvis live server åbner din browser og viser `index.html` virker det!
8. Prøv at ændre i `index.html` og `style.css` og læg mærke til at browseren opdaterer hver gang du gemmer.

## Opgave 3 - BEM markup øvelse

_team opgave: 120 minutter_

### Information

I er sikkert super gode til css og semantisk HTML, muligvis også sass/scss ? Men har i nogensinde brugt BEM ?  
Et af de store problemer med css er at det hurtigt bliver rodet hvis ikke du er meget stringent med din navngivning og har et godt system til at holde orden.

BEM står for _Block Element Modifier_ og forsøger at løse problemet ved at implemetere et simpelt system til at strukturere css.  
Mens i laver denne øvelse så debatter følgende:

- Hvordan passer jeres viden om semantisk HTML med BEM ?
- Hvilke problemer synes i BEM løser

### Instruktioner

<img src="assets/images/bem_is_useful.png" alt="bem is design" />

_Design: Sass is useful_

1. Læs om BEM [http://getbem.com/naming/](http://getbem.com/naming/)
2. Se video om BEM fra "Kevin Powell" - _Why I use the BEM naming convention for my CSS_ [https://youtu.be/SLjHSVwXYq4](https://youtu.be/SLjHSVwXYq4)
3. Lav en plan for hvordan i vil strukture HTML for ovenstående design, med brug af semantiske HTML tags [https://developer.mozilla.org/en-US/docs/Glossary/semantics#semantics_in_html](https://developer.mozilla.org/en-US/docs/Glossary/semantics#semantics_in_html)
4. Lav en plan for hvordan i vil style siden med BEM.
5. Implementer HTML og CSS ifølge jeres plan (dette trin kan også gøres som pair programming [https://www.geeksforgeeks.org/pair-programming/](https://www.geeksforgeeks.org/pair-programming/))
6. Upload jeres løsning til en mappe i jeres git remote repo

## opgave 4 - installer sass compiler (css preprocessor)

_individuel opgave: 45 minutter_

### Information

For at kunne bruge sass i dit projekt er du nødt til at installere sass compileren, heldigvis er det super nemt fordi den er tilgængelig som en NPM pakke.
Når vi starter på VUE så skal vi også bruge sass, som installeres sammen med VUE når du bruger vue cli (mere om det senere).  
På den måde er den her øvelse både en forløber for hvad du skal lave i VUE og samtidig giver den en forståelse for hvad VUE installerer som en del af et VUE projekt.

### Instruktioner

1. Lav en kopi af løsningen fra opgave 3 og lav om i projektets mappe struktur så det ligner nedenstående.  
   Alternativt kan du lave et nyt node.js projekt ved at gentage øvelse 1.
   ```text
   sass-compile/
   ├── assets/
   │   ├── css/
   │   │   └── style.css
   │   └── scss/
   │       ├── assets/
   │       │   ├── _variables.scss
   │       │   └── _header.scss
   │       │   └── tilføj selv flere scss filer efter behov
   │       └── style.scss
   ├── node-modules/
   ├── package.json
   └── index.html
   ```
2. Installer nu npm Sass pakken. I din terminal skriv `npm install sass --save-dev`
3. Opdater `package.json` script afsnittet til:
   ```json
   "scripts": {
       "sass-dev": "sass --watch --update --style=expanded assets/scss:assets/css",
       "sass-prod": "sass --no-source-map --style=compressed assets/scss:assets/css",
       "live-server": "live-server"
     }
   ```
4. Fra en terminal skriv `npm run sass-dev` som starter sass kompileren i _watch mode_. Det betyder at alle ændringer i .scss filer får kompileren til at lave en opdateret `style.css`

**_Bonus spørgsmål_**

- _Hvad sker der hvis du kører kommandoen `npm run sass-dev` ?_
- _Hvilket dataformat er `package.json` ?_

## Opgave 5 - BEM og SCSS

_individuel opgave: 180 minutter_

### Information

Nu har du fået en ide om hvad BEM er og forhåbentlig kan du se meningen med at strukturere dine stylesheets med BEM.  
Men kan det blive bedre ?  
Ja, det kan det. Hvis du bruger BEM i kombination med sass kan du lave nogle meget modulære stylesheets hvor du sikrer konsistens og læsbarhed, samtidig med at du undgår at gentage dig selv, du ved DRY...

Når du skal til at omskrive din css til scss kan det være en god ide at have sass dokumentationen åben i din browser. Den er her [https://sass-lang.com/documentation](https://sass-lang.com/documentation)

### Instruktioner

1. Læs om css preprocessors [https://developer.mozilla.org/en-US/docs/Glossary/CSS_preprocessor](https://developer.mozilla.org/en-US/docs/Glossary/CSS_preprocessor)
2. Læs introduktionen til sass [https://sass-lang.com/guide](https://sass-lang.com/guide)
3. Nu skal du bruge sass til at omskrive `style.css` fra opgave 3\. Prøv så mange muligheder fra sass som muligt så du lærer meget :-)  
   **_#Partials #Variables #Nesting #Modules #Mixins #Extend #Operators_**
4. Gem løbende din kode, i en mappe i jeres fælles gitlab/github projekt. På den måde får du en refresher på dine git skills og det er også nemmere at bede andre kigge på din kode.

## Øvelse 6 - Markdown

_Individuel opgave: 45 minutter_

### Information

Det du læser lige nu er skrevet som tekstfil og derefter renderet til html og pdf ved hjælp af en CI/CD pipeline på Gitlab.  
Tekstfilens format er Markdown. Markdown har en meget simpel formaterings syntax og renderes default på mange platforme som f.eks github, udover det bruges Markdown bruges mange andre steder.

Endelig er markdown, efter lidt tilvænning, virkelig brugbart til at tage noter, både på studiet eller når i arbejder med andre ting hvor i har behov for at notere.

### Instruktioner

1. Læs om markdown og hvad du kan bruge det til [https://www.markdownguide.org/getting-started](https://www.markdownguide.org/getting-started)
2. Kig på markdown syntax i dette cheatsheet [https://www.markdownguide.org/cheat-sheet](https://www.markdownguide.org/cheat-sheet)
3. Undersøg hvordan du kan previewe markdown i din favorit editor.
4. Lav en test markdown fil `.md` og prøv mulighederne af ved hjælp af cheat sheetet fra punkt 2. (det kan godt være du skal søge lidt ekstra på nettet for at gøre alt nedenstående)
   - Lav 3 forskellige overskrifter
   - Lav et link
   - Indsæt et billede (link til et billede)
   - Lav en 4\*4 tabel med første kolonne venstre justeret, kolonne 2 og 3 centreret og kolonne 4 højre justeret.
   - Lav en unordered list (bullet points) med 3 niveauer
   - Lav en ordered list med 2 niveauer
   - Lav fed tekst og italic tekst. Kan man lave fed italic tekst samtidig ?
   - Lav en code block og angiv det sprog koden er skrevet i.
   - Find selv på mere hvis du har ekstra tid. Kig evt. på bonus herunder.

_Bonus_ [https://obsidian.md/](https://obsidian.md/) er ret kool til at holde styr på dine markdown filer lokalt, du kan selvfølgelig gemme din vault remote hvor du ønsker det. Hvem ved, måske bliver det måden du laver notater igennem uddannelsen ?  
[https://joplinapp.org/](https://joplinapp.org/) er også en mulighed som er lidt mindre manuel i forhold til obsidian.
