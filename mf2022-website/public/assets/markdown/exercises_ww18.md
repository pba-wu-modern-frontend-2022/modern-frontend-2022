---
name: Opgaver uge 18
week: 17
tags:
  - Backend
  - Nodejs
  - CORS
  - Express.js
---

# Opgaver uge 18

I denne uge skal I kigge på backend. Formålet er forstå hvordan en backend fungerer, med særligt fokus på Application Programming Interface (API).
Derfor vil denne uge fokusere på at lave en lille simpel backend.

Inden vi går igang, vil jeg gerne fremvise VueJS router.

## Opgave 1 - ExpressJS

_Individuel_

### Information

I NodeJS er det framework der hedder Express.js et meget brugt framework. I denne første opgave skal I prøve at arbejde med Express.js.
Har I allerede arbejdet med Express.js, så spring denne opgave over.

### Instruktioner

1. Følg deres officielle tutorial til opsætning af en basic Express.js applikation [Express.js](https://expressjs.com), hvor I skal følge deres getting started.
2. Når ovenstående er gennemgået, prøv nu følge database integrationsvejledningen [Database Integration](https://expressjs.com/en/guide/database-integration.html)

## Opgave 2 - API Server

_Individuel_

### Information

Selve API strukturen kan håndteres relativt nemt med Express.js, hvilket er med til at have gjort frameworket utroligt populært.

### Instruktioner

1. Gennemgå denne tutorial: [How to create a REST API with Express.js in Node.js](https://www.robinwieruch.de/node-express-server-rest-api/)
  * Husk på at det er helt fint hvis I bruger Postman i stedet for cURL, som tutorialen foreslår at I installerer og denne gang er der en lille introduktion til Postman.
  * [Getting Started With Postman](https://www.youtube.com/watch?v=q78_AJBGrVw)
2. Den statiske udgave af en API server fungerer som ovenstående. Prøv nu at finde en løsning til at koble databasen sammen med ovenstående tutorial API server.
3. EKSTRA: Prøv at implementere MVC i jeres kodebase.

## Arbejde videre

Der er 2 retninger, enten er I velkomne til at forsætte projekt og ellers kan I se på disse tutorials:

* [Express Essential Training](https://www.linkedin.com/learning/express-essential-training-14539342)
* [Building RESTful APIs with Node.js and Express](https://www.linkedin.com/learning/building-restful-apis-with-node-js-and-express)
