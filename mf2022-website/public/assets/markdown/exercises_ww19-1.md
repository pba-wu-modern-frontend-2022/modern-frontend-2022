---
name: Opgaver uge 19/1
week: 19/1
tags:
  - API
  - RESTful
  - JSON
---

# Opgaver uge 19/1

I denne uge kigger vi på brugen af API services og deres strukturelle opsætning, med et særligt fokus på RESTful.

## Gennemgå jeres RESTful API struktur

_Grupper m. underviser_

### Information

Sammen kan vi kigge på jeres API struktur og evaluere hvordan den fungerer på nuværende tidspunkt.

## Arbejde på opgave

_Grupper_

### Information

Arbejde videre med opgaver

## Links

* [What is REST](https://restfulapi.net/), denne webside indeholder guides, techs og FAQ's om REST
