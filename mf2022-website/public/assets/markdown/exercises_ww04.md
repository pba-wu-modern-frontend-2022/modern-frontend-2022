---
title: Opgaver uge 4
week: 4
tags:
  - Spørgeskema
  - Teams
  - MFD Projekt
  - Frontend
  - GIT
---

# Opgaver uge 4

Tider i opgaver er vejledende.

## Opgave 1 - Spørgeskema

_individuel opgave: 15-30 minutter_

### Information

For at lære jeres forudsætninger at kende vil jeg (nisi) gerne vide hvordan i subjektivt vurderer jeres faglige evner.  
Til det har jeg lavet et spørgeskema som i alle skal udfylde.

**Spørgeskemaet kan kun tilgås ved at bruge din UCL konto's user og pass**

### Instruktioner

1. Udfyld spørgeskemaet [https://forms.gle/Jk1twPM4vT9Xu9Gd9](https://forms.gle/Jk1twPM4vT9Xu9Gd9)

## Opgave 2 - Teams

_individuel/team opgave: tid er angivet i opgavens instruktioner_

### Information

Gennem 2. semester skal i samarbejde i teams.  
Denne opgave kan hjælpe jer til at danne et nyt team eller evaluere jeres eksisterende team fra 1. semester.  
Det anbefales at et team dækker alle belbins 9 team roller, det øger chancen for et velfungerende og højtydende team.

Det er godt at være opmærksom på teamets manglende team roller, især hvis der opstår konflikter vil teamets manglende kompetencer ofte kunne identificeres som konfliktens ophav.  
På den måde bliver konflikter ikke personlige og kan løses ved forståelse gennem et 3. fælles.

I har muligvis allerede et godt team fra 1. semester som i gerne vil fortsætte med og det er også ok.  
Vi vil alligevel bede jer, ved at gennemføre denne opgave, at evaluere jeres team.

_Anbefalet øvrig litteratur_ [https://dpf.dk/produkt/studiegruppen-2-udgave/](https://dpf.dk/produkt/studiegruppen-2-udgave/)

### Instruktioner

1. Gennemfør "fra gruppe til team" opgaven, som er tilgængelig på itslearning, i Modern Frontend Development F22 Planen. _30-40 minutter_
2. Overvej og skriv 2-3 belbin roller som du mener passer til dig, skriv rollerne ned så du kan huske dem. _20 minutter_
3. Sæt dig sammen med 3 tilfældige medstuderende og lav en bordet rundt hvor i præsenterer jeres roller fra punkt 2, skriv rollerne ned. Dækker i alle belbins 9 roller ? Er der roller i har for mange af? _15 minutter_
4. Udfyld oplysninger fra punkt 3 i google dokumentet [https://docs.google.com/document/d/1M2JEnv71nqK5GroJmJvUCmHT1RfGJXCLmoJRaydzTIQ/edit?usp=sharing](https://docs.google.com/document/d/1M2JEnv71nqK5GroJmJvUCmHT1RfGJXCLmoJRaydzTIQ/edit?usp=sharing) **Dokumentet kan kun tilgås ved at bruge din UCL konto's user og pass** _15 minutter_
5. Kig i google dokumentet og se om et af de andre teams har en overrepræsentation af roller som i mangler. _10 minutter_
6. Hvis punkt 5 giver anledning til det, snak med de andre teams og juster sammensætningen af jeres teams så i dækker belbins team roller så godt som muligt. _10 minutter_
7. Opdater google dokumentet hvis i har ændret team sammensætning _5 minutter_

## Opgave 3 - MFD Projekt

_team opgave: tid er angivet i opgavens instruktioner_

### Information

I Modern frontend development skal i arbejde på et projekt som sammen med en rapport danner grundlag for fagets eksamen.  
Derfor er det vigtigt at i kender kravene til projektet fra start.

### Instruktioner

1. Læs læringsmålene for Modern Frontend på itslearning - Beslut i teamet om i har spørgsmål til læringsmålene. Skriv spørgsmål ned. _15 minutter_
2. Læs om Afsluttende prøve. Beslut i teamet om i har spørgsmål til indholdet. Skriv spørgsmål ned. _15 minutter_
3. Opsamling på klassen baseret på jeres spørgsmål. _15-30 minutter_

## Opgave 4 - GIT og repository til MFD projekt

_team opgave: 60 minutter_

### Information

I Modern Frontend development forventes det at i bruger git til versionsstyring samt en DevOps platform til samarbejde og eventuelt CI/CD og hosting.

Vi bruger Gitlab, men i kan selv vælge hvilken provider i vil benytte, f.eks Github eller andet.  
Vi har som krav (også til eksamen) at uanset provider skal jeres repo være public og linket skal deles med os.

Hvis du trænger til at genopfriske din git viden har jeg lavet en codelab der dækker de mest basale git kommandoer samt lidt vejledning i brugen af gitlab.  
W3Schools har også en udemærket tutorial.  
Find selv flere ressourcer hvis du har brug for det.

- Gitlab daily workflow [https://eal-itt.gitlab.io/gitlab_daily_workflow/index.html](https://eal-itt.gitlab.io/gitlab_daily_workflow/index.html)

- W3Schools GIT tutorial [https://www.w3schools.com/git/default.asp](https://www.w3schools.com/git/default.asp)

### Instruktioner

1. Sørg for at have GIT installeret på din computer, check evt. med `git --version` i en terminal. Hvis du ikke har det installeret kan det hentes på [https://git-scm.com/downloads](https://git-scm.com/downloads)
2. I jeres team opret et repository til jeres MFD projekt på en git platform (Gitlab, Github, bitbucket etc.)
3. Sæt repo'et public og test det i en incognito fane.
4. Test at alle i teamet kan tilgå repo'et med GIT.  
   Test clone/pull/push osv. med en simpel text fil hos alle team medlemmer.
5. Skriv link til repo'et i google dokumentet [https://docs.google.com/document/d/1M2JEnv71nqK5GroJmJvUCmHT1RfGJXCLmoJRaydzTIQ/edit?usp=sharing](https://docs.google.com/document/d/1M2JEnv71nqK5GroJmJvUCmHT1RfGJXCLmoJRaydzTIQ/edit?usp=sharing)

## Opgave 5 - Research frontend

### Information

I denne opgave skal i researche hvilke frontend teknologier der benyttes mest på nuværende tidspunkt.

Formålet med opgaven er at få et generelt overblik over emnet **Modern Frontend Development**

- Det er værdifuldt for jer i forhold til at arbejde med faget og dets teknologier.
- Det skaber fælles forståelse i teamet.
- Det kan være brugbart i f.eks. indledningen i jeres rapport.

Opgaven er en vidensopsamlings opgave og har defor ikke noget klart defineret facit, hvis opgaven giver anledning til spørgsmål, så tag dem meget gerne op på klassen.

_team opgave: resten af dagen_

### Instruktioner

1. Det anbefales at lave et dokument som i deler i jeres team, dokumentet bruger i til at notere den information i samler gennem semesteret i Modern Frontend Development.
2. Forslag til spørgsmål i kan bruge i jeres søgning

   - Hvilke frontend frameworks findes ?
   - Hvad er komponent baserede frameworks ?
   - Hvilke build værktøjer bruger de forskellige frameworks ?
   - Hvilke frontend development kompetencer er mest efterspurgt på f.eks jobindex ?
   - Hvad er lighederne mellem de forskellige frameworks ?
   - Hvilke programmeringssprog er mest anvendt i frontend development i 2022 ?
   - Hvilke markupsprog er mest anvendt i frontend development i 2022 ?
   - Hvad skal man kunne for at arbejde med Frontend Development ?
   - _Find selv flere spørgsmål og research dem_

3. Inkluder et link til dokumentet i jeres git repo så i løbende kan opdatere det.
