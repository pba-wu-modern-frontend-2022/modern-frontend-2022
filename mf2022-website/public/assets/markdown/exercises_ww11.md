---
name: Opgaver uge 11
week: 11
tags:
  - JSON Server
  - JS Fetch
  - Async/await
  - Promises
  - HTTP(s) request
  - Asynkron javascript
---

# Opgaver uge 11

## Opgave 1 - Fetching data

_individuelt: 120 minutter_

### Information

Denne øvelse handler om at bruge JS Fetch API til at lave http(s) requests til en server.  
Fetch API er en indbygget API i JS som bruger asynkron javascript, fordi hentning af data tager et ukendt tidsrum.  
Når data returneres kan det viderebehandles og parses som f.eks JSON.

Fetch API er, som sagt, asynkron javascript som er et helt emne for sig selv, netninja har en playliste (Asynchronous JavaScript Tutorial) om asynkron javascript, som måske kan være værdifuld hvis du ikke kender til callback functions, promises, HTTP requests og Async/Await [https://youtube.com/playlist?list=PL4cUxeGkcC9jx2TTZk3IGWKSbtugYdrlu](https://youtube.com/playlist?list=PL4cUxeGkcC9jx2TTZk3IGWKSbtugYdrlu)

En af videoerne er specifikt om fetch api, den skal du som minimum se hvis du ikke har brugt fetch før: [https://youtu.be/drK6mdA9d_M](https://youtu.be/drK6mdA9d_M)

Skrevet dokumentation (med gode eksempler) for Fetch API er her: [https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)

I _Net Ninja - Vue JS 3 Tutorial for Beginners #9 - Fetching Data_ bruges en npm pakke som hedder json-server, net-ninja installerer den lokalt. For at få det til at vorke på min maskine var jeg nødt til at køre json-server med npx, for eksempel `npx json-server --watch data/db.json`

json-server dokumentationen er her: [https://www.npmjs.com/package/json-server](https://www.npmjs.com/package/json-server)
[https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/) er en _fake_ api som også kan bruges til test af fetch.

### Instruktioner

1. Lav en ny vue applikation eller byg videre på en af dem du har lavet i tidligere uger.
2. Se og udfør det samme som vises i video 8 (lesson 54 - 56).  
   _Net Ninja - Vue JS 3 Tutorial for Beginners #9 - Fetching Data_ [https://youtu.be/juocv4AtrHo](https://youtu.be/juocv4AtrHo)
3. commit/push dit arbejde til gitlab/github mens du arbejder. Husk at skrive rigeligt med kommentarer i din kode så du kan bruge hvad du har lært senere.

## Opgave 2 - Projekt arbejde

_team: 300 minutter_

### Information

Egentlig mest en placeholder opgave så i husker at arbejde videre på jeres projekt, det vil være naturligt at få implementeret fetch af data fra jeres valgte API.

Husk også at arbejde hen imod jeres _deploy prototype_ som i skal fremvise på klassen den 31. Marts.

### Instruktioner

1. Dan jer et overblik over hvor i er henne i projektet og hvad målet for dagens arbejde er.
2. Opret tasks/issues der sikrer at i får planlagt implementering af at hente data fra en API (brug jeres projektplan her)
3. Fordel tasks/issues
4. Arbejd på aftalte tasks/issues
