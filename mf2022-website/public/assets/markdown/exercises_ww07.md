---
name: Opgaver uge 7
week: 7
tags:
  - VueJS
  - Vue CLI
  - Første vue applikation
  - Projekt arbejde
---

# Opgaver uge 7

I denne uge fortsætter vi med Vue, i skal installere og bruge Vue cli til at lave jeres første vue applikation.  
Det er også vigtigt at i som team har aftaler på plads om hvordan i beskriver og fordeler arbejde imellem jer.

Husk at det ikke handler om at udføre opgaverne hurtigst muligt, det handler om at forstå mest muligt. Så mens du udfører opgaverne kan det være nødvendigt at undersøge de ting du er nysgerring på, eller undrer dig over, nærmere. Det kan du gøre ved at søge efter yderligere materiale, eller stoppe op og snakke med dit team, om hvordan de forstår en ting eller et emne.

I skal sidde sammen med jeres team og lave opgaverne, også de opgaver som er individuelle.  
Grunden til at i skal sidde sammen er så i kan hjælpe hinanden hvis noget er svært at forstå.

## Opgave 1 - Vue CLI

_individuel opgave: 240 minutter_

### Information

Denne opgave forudsætter at du har set og udført _opgave 2 - Vue overblik_, fra uge 6.

I sidste uge lærte du at bruge vue via cdn, det var for at få en ide om hvad vue er og hvad det kan bruges til.  
Når du udvikler vue applikationer vil du typisk starte med at initiere et projekt med vue CLI.

Når du initierer et projekt med vue CLI og kommandoen `vue create my-project` kan du vælge at tilføje forskellige værktøjer (plugins) som en del af dit projekt, f.eks sass compileren som du brugte for et par uger siden, babel, eslint mm.

Her kan du se hvordan det ser ud når du laver et nyt vue projekt med vue cli [https://cli.vuejs.org/guide/creating-a-project.html#vue-create](https://cli.vuejs.org/guide/creating-a-project.html#vue-create)

Husk jævnligt at committe din kode med git, husk at pushe din kode når du er færdig med opgaven, eller dagen er slut.

### Instruktioner

1. Tag et kig på vue cli og dan dig et overblik over hvad det er [https://cli.vuejs.org/guide/](https://cli.vuejs.org/guide/) klik dig rundt på siden og kig også på andre ting end guiden jeg linker til.
2. Brug vue cli i roden af dit teams github/gitlab projekt, det laver en ny mappe i projektet til denne øvelse.  
   Husk jævnligt at committe din kode med git og husk at pushe din kode når du er færdig med opgaven, eller dagen er slut..  
    Vær opmærksom på at vue cli måske opretter dit projekt som et git projekt, det kan du checke ved at kigge om der i dit projekt er en skjult mappe der hedder `.git`.  
    Hvis det er tilfældet så slet den, du gemmer jo dine filer i en mappe på et allerede eksisterende git projekt (teamets).
3. Den første video om vue CLI du skal se og udføre er video 4 i net ninja serien. Det vil give dig et overblik over vue cli, vue files, templates og template refs.  
   **_Vue JS 3 Tutorial for Beginners #4 - The Vue CLI & Bigger Projects (part 1)_** [https://youtu.be/GWRvrSqnFbM](https://youtu.be/GWRvrSqnFbM)
4. Det næste du skal se og udføre vises i video 5 som er del 2 af vue cli videorne. Det vil give dig et overblik over scoped og global css, props, emitting custom events, click event modifiers, slots og teleport.  
   **_Vue JS 3 Tutorial for Beginners #5 - The Vue CLI & Bigger Projects (part 2)_** [https://youtu.be/KM1U6DqZf8M](https://youtu.be/KM1U6DqZf8M)

## Opgave 2 - Første Vue projekt

_individuel opgave: 80 minutter_

### Information

For at øve hvad du indtil videre har lært om vue skal du i denne øvelse lave et lille spil.  
Det er måske ikke det du typisk vil bruge vue til som web udvikler men det giver en god tur rundt om det du har lært i denne og sidste uge.

Husk jævnligt at committe din kode med git, husk at pushe din kode når du er færdig med opgaven.

### Instruktioner

1. Brug vue cli i roden af dit teams github/gitlab projekt, det laver en ny mappe i projektet til denne øvelse.  
   Husk jævnligt at committe din kode med git og husk at pushe din kode når du er færdig med opgaven, eller dagen er slut.  
    Vær opmærksom på at vue cli måske opretter dit projekt som et git projekt, det kan du checke ved at kigge om der i dit projekt er en skjult mappe der hedder .git. Hvis det er tilfældet så slet den, du gemmer jo dine filer i en mappe på et allerede eksisterende git projekt (teamets).
2. Se og udfør video 6, som er en vue applikation, der træner dig i de ting du allerede har lært i vue.  
   Du skal lave et lille spil spil som viser din reaktions tid  
    **_Vue JS 3 Tutorial for Beginners #6 - Build a Reaction Timer Game_** [https://youtu.be/bc6czIBLKTg](https://youtu.be/bc6czIBLKTg)

## Opgave 3 - Projektarbejde

_team opgave: resten af dagen, i planlægger selv tid i de opgaver i aftaler i teamet_

### Information

Indtil uge 8 vil Kenneth og jeg kigge jeres projekt planer igennem. I uge 8 får i feedback på projekt planen og der skal muligvis justeres lidt her og der.  
Det betyder dog ikke at i skal vente med at starte projekt arbejdet, det er helt klart muligt at lave nogen af de mere generiske ting i projektet, opsætning af kanban board, måske vil i lave en wiki til projektet?, vue skal i bruge og nu kan i starte projektet med vue CLI osv.

Men hvordan kan i sammen beskrive opgaver som ikke kun i, men også andre med interesse i projektet, som undervisere eller kunder, kan forstå ?

### Instruktioner

1. Opsæt jeres kanban board på enten gitlab eller github. Jeg vil kraftigt anbefale at i kun bruger et par kolonner, f.eks. `open`, `to do`, `doing` og `done`
   Læs om gitlab issue board her [https://docs.gitlab.com/ee/user/project/issue_board.html](https://docs.gitlab.com/ee/user/project/issue_board.html)  
   Læs om github projects [https://docs.github.com/en/issues/trying-out-the-new-projects-experience/about-projects](https://docs.github.com/en/issues/trying-out-the-new-projects-experience/about-projects)
2. I jeres team, få et overblik over hvad i allerede nu kan lave i projektet, samtidig med at i tager hensyn til at jeres projektplan muligvis ændrer sig ifht. vores feedback i næste uge. Skriv opgaverne som en liste til at starte med. Bare overskrifter.
3. Læs om SMART goals og snak i teamet om hvordan i kan bruge det når i planlægger projektopgaver. Målet er at i bliver enigen om hvordan opgaver i projektet skal beskrives så alle kan forstå dem (alle intressanter) og alle i teamet er i stand til at arbejde med en given opgave.
   SMART stammer fra en [artikel](https://community.mis.temple.edu/mis0855002fall2015/files/2015/10/S.M.A.R.T-Way-Management-Review.pdf) skrevet af _George T. Doran_ i 1981 og er et akronym for:
   - **S**pecific - hvad handler opgaven om, hvad forbedrer den ifht. projektet ?
   - **M**easurable - hvordan måles opgavens fremdrift, delmål eller anden måling ?
   - **A**ssignable - hvem skal udføre opgaven ?
   - **R**ealistic - hvilke resultater er realistiske at forvente af denne opgave ?
   - **T**ime-related - hvornår skal ogaven være fuldført ?
     En uddybende artikel kan findes her: [https://project-management.com/smart-goals/](https://project-management.com/smart-goals/)
4. Opret de første opgaver i jeres kanban board, i open kolonnen.
5. Aftal hvilke i kan nå at starte på i dag, 1 opgave pr. team medlem. Flyt opgaverne til todo kolonnen.
6. Når det enkelte team medlem begynder på en opgave flytter medlemmet opgaven til doing kolonnen.
7. Når et team medlem er færdig med en opgave så bruges lidt tid på at briefe hele teamet med henblik på at vidensdele blandt team medlemmerne.
