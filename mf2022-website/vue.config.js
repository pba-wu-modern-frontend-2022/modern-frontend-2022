module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production' ? '/modern-frontend-2022/' : '/',
};
