import { createRouter, createWebHashHistory } from "vue-router";
import { publicPath } from "/vue.config.js";
// import Index from "../views/Index.vue";
import CardView from "../components/CardView.vue";
import Article from "../components/Article.vue";
import NotFound from "../views/NotFound.vue";

/*
 * If an endpoint have a name it will appear in the navigation
 */
const routes = [
  {
    path: "/",
    name: "Home",
    component: CardView,
  },
  {
    path: "/article/:id",
    name: "Article",
    component: Article,
    props: true,
  },
  // catch all not found routes
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHashHistory(publicPath),
  routes,
});

export default router;
