import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { marked } from 'marked';

//import VueCodeHighlight from "vue-code-highlight";
marked.setOptions({
  renderer: new marked.Renderer(),
  highlight: function (code, lang) {
    const hljs = require('highlight.js');
    const language = hljs.getLanguage(lang) ? lang : 'plaintext';
    return hljs.highlight(code, { language }).value;
    //return hljs.highlightAuto(code).value;
  },
  langPrefix: 'hljs language-', // highlight.js css expects a top-level 'hljs' class.
  pedantic: false,
  gfm: true,
  breaks: false,
  sanitize: false,
  smartLists: true,
  smartypants: false,
  xhtml: false,
});
// marked.use(markedImages);
// use marked.js globally, ref with this.md in components.
const markedMixin = {
  methods: {
    md: function (input) {
      //console.debug("md mixin loaded");
      return marked.parse(input);
    },
  },
};

// import md files paths
// const md_files = require.context(
//   "@/../public/assets/markdown/",
//   true,
//   /^.*\.md$/
// );

//console.log(md_files.keys());

const app = createApp(App);
app.use(router);
//app.use(VueCodeHighlight); //registers the v-highlight directive
//app.provide("$mdFiles", md_files);
app.mixin(markedMixin);
app.mount('#app');
