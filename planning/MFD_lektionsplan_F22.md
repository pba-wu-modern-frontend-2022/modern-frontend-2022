# Lektionsplan

# Fagplan

Fagplanen indeholder følgende dele. en ugeplan/fagplan, et overblik over studieaktiviteterne og generel information om faget, modulet eller projektet.

- Uddannelse, klasse og semester: Webudvikling, WUOE211, 22F
- Navn på underviser og dato for udfyldelse af dokumentet: NSIS/KJCL, 2022-01-14
- Titel på fag, modul eller projekt og ECTS: Modern Frontend Development, 10 ECTS
- Nødvendig lekture, bøger eller instruktioner og anden baggrunds materiale:

Besøg fagets opgaveside på [Modern Frontend Development](https://pba-wu-modern-frontend-2022.gitlab.io/modern-frontend-2022/#/) for at se alle tilgængelige opgaver.

| Underviser | Uge | Indhold |
| :--------: | :-: | :------ |
|            |

Emnerne i de enkelte uger opdateres efter behov.

# General information om faget, modulet eller kurset

Formålet med kurset er at give de studerende et indblik i moderne frontend udvikling.

## Læringsmål (fra studieordningen)

**Viden**

Efter afslutning af kurset skal de studerende have erhvervet viden om:

- Client-side webapplikationsarkitektur (DB, frontend, API/Backend)
- Style processing og naming conventions (SCSS, LESS, Stylus, BEM)
- Frontend eco system (module bundling, techstack (ie. Mern), frameworks, typescript) Find god grafik
- Frontend workflows (webpack, gulp, javascript)
- Frontend frameworks (VUE, Andre FE frameworks)
- Kvalitetssikring (Testing pyramid, Dokumentation, GIT, branching strategier, code review, Jest. CI/CD? se Bernie slides ) Gitlab eller Github ?

**Færdigheder**

Efter afslutning af kurset skal de studerende have erhvervet færdigheder til:

- Planlægge, udvikle og implementere/idriftsætte client-side web applikationer baseret på konkrete udviklingsønsker (skal hostes live/deploy, se krav til projekt)
- Vælge egnede værktøjer og metoder til implementering af client-side web applikationer (Framework, bundler, preprocessor. Valg skal begrundes)
- Implementere cross-platform web brugergrænseflader (Responsive, PWA (KJCL))
- Implemetere client-side logik (Routing eller state manager)
- Anvende metoder til kvalitetssikring af client-side web applikationer (se test i videns del)

**Kompetencer**

Efter afslutning af kurset skal de studerende have erhvervet kompetencer til at:

- Analysere komplekse udviklingsønsker til at vælge og anvende egnede workflows, metoder, værktøjer og frameworks til implementering af cross-platform, performant, vedligeholdelsesvenlig (navngivning, dokumentation) og dynamiske client-side web applikationer (modsat af statisk, API)

## Indhold

Formålet med faget er at udvikle kompetencer til at skabe webapplikationer ved hjælp af moderne frontend teknologier, arbejdsgange og værktøjer. Det beskriver og diskuterer den nuværende praksis med frontend engineering.

Der er fokus på at arbejde med de nyeste teknologier inden for frontend udvikling:

Frontend/Javascript framework, SASS/SCSS, Semantic HTML, SPA, PWA, GIT og testing.

## Metode

Der vil være en række oplæg og diskussioner på klassen. Det meste af arbejdet vil være individuelt eller i små grupp

## Udstyr

- Computer
- Server til at hoste web applikation (1 pr. gruppe)

### Prøveform

**Krav til produkt for prøven**

De studerende udarbejder i grupper en webapplikation og en rapport på minimum 10 og maksimum 15 normalsider for en gruppe.  
Gruppen må højest være på 4 personer.  
Der kan i særlige tilfælde dispenseres, så en gruppe kan bestå af en person.

For at kunne vurdere de studerendes individuelle præstation skal det fremgå af indholdsfortegnelsen for rapporten hvilke afsnit, der er udarbejdet af den enkelte studerende.

Webapplikation og kode, som er udviklet af de studerende, skal være tilgængeligt online for censor og eksamintaor.

**Prøveform**

Prøven er individuel og mundtlig baseret på en skriftlig rapport og en udarbejdet webapplikation.  
Der afsættes 30 minutter pr. eksaminand fordelt på 10 minutters præsentation, 15 minutters eksamination og 5 minutters votering.

**Bedømmelse**

Der gives èn individuel karakter efter 7-trinsskalaen ud fra en helhedsvurdering af den afleverede rapport, web applikation, samt den mundtlige præstation.

Som følge af engelsksproget undervisning i det lokale fagelement, gennemføres prøven på engelsk.

Prøven er med ekstern bedømmelse.

**Forudsætning for deltagelse i prøven**

Som forudsætning for detagelse i prøven, skal den studeernede deltage i en planlagt aktivitet i faget Modern Frontend Development. Se semesterbeskrivelsen for en nærmere beskrivelse af forudsætningskravet og den praktiske afvikling heraf.  
Hvis forudsætningen ikke opfyldes, er den studerene ikke indstillet til prøven og har samtidig brugt et prøveforsøg.

## Studie aktivitets model

Fagets aktiviteter er inddelt i 4 kateogrier. K1, K2, K3, og K4.
Kategoriernere er defineret i Danske professionshøjskolers [studieaktivitets model](https://danskeprofessionshøjskoler.dk/studieaktivitetsmodel-2/)

Der er tre formål med studieaktivitetsmodellen:

1. Modellen skal bruges til at synliggøre, at de samlede studieaktiviteter reelt udgør en fuldtidsuddannelse.
2. Modellen synliggør, at en professions- og erhvervsrettet uddannelse omfatter forskellige typer af studieaktiviteter.
3. Modellen danner grundlag for kommunikation og forventningsafstemning mellem studerende og undervisere i forhold til studieintensitet og i forhold til roller og ansvar i de forskellige typer af studieaktiviteter.

Læs [Læs vejledning for studerende](https://xn--danskeprofessionshjskoler-xtc.dk/wp-content/uploads/2020/01/Vejledning_studieaktivitetsmodel_studerende.pdf) for at lære studie aktivitets modellen at kende.

![study activity model](Study_Activity_Model.png)

## Anden generel information

Ingen pt.
