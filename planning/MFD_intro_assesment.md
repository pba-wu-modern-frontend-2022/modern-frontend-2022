# MFD Introdag studerendes forudsætninger

## Link til forms

- [https://forms.gle/Jk1twPM4vT9Xu9Gd9](https://forms.gle/Jk1twPM4vT9Xu9Gd9)

## Emner

- SPA
- GIT
- CI/CD
- Devops platforms (Github, Gitlab, Bitbucket etc.)
- CSS Preprocessors (SASS/SCSS)
- Semantisk HTML
- Javascript
- Ecmascript
- Typescript
- Transpilers (babel)
- Javascript async/await
- Javascript array functions (reduce, map, filter)
- Javascript Spread, object destructuring
- Node.js
- NPM
- Vue 3
- Vue CLI
- Bundlers (Webpack)
- Component based frontend frameworks (Vue 3, React, Angular, Svelte etc.)
- Vue components
- Vue props og emit
- Vue attribute binding
- Vue directives
- Vue router
- Software testing (unit, integration)
- Useability testing
- Vue forms & validation
- Vue filter
- Vue state management (VUEX)
- API anvendelse
- API implementering
- JSON
- PWA (manifest, service worker)
