# Modern Frontend Development - Forår 2022

## Overblik

| Uge       | Emner                                                                                                    | Opgaver                                          | Underviser |
| :-------- | :------------------------------------------------------------------------------------------------------- | :----------------------------------------------- | :--------- |
| 4 (27.1)  | Introduktion, Hvad er frontend + arkitektur, fagprojekt, projektkrav/eksamen, Git, teams, Spørgeskema    | Opsæt Git og Gitlab, spil ohmygit, MDAM på besøg | KJCL/NISI  |
| 5 (03.2)  | Style processing og naming conventions (CSS Preprocessors), projektbeskrivelse, HTML semantics           | projektbeskrivelse, scss tutorial                | NISI       |
| 6 (10.2)  | Javascript, Typescript, transpilers, ECMAscript, DOM                                                     | Projektarbejde, JS opgaver                       | NISI       |
| 7 (17.2)  | Node, NPM, JS frameworks, VueJS, Vue CLI, Vue ressources, Bundlers                                       | Setup environment, First VUE app, Projektarbejde | NISI       |
| 8 (24.2)  | JS Array Functions: Reduce, Map, Filter                                                                  | Projektarbejde                                   | NISI       |
| 9 (03.3)  | JS - Spread Syntax, Destructuring Objects, async/await                                                   | JS opgaver, Projektarbejde                       | NISI       |
| 10 (10.3) | Single File Components, Single Page Applications, attribute binding, directives                          | Projektarbejde                                   | NISI       |
| 11 (17.3) | Vue Templating, Vue Router, components, Props, Slot                                                      | Projektarbejde                                   | NISI       |
| 12 (24.3) | Forbered deploy prototype                                                                                | Projektarbejde                                   | NISI       |
| 13 (31.3) | OLA: præsentation af deploy prototype                                                                    |                                                  | KJCL/NISI  |
| 14 (07.4) | MDAM testing                                                                                             |                                                  | MDAM       |
| 15 (14.4) | PÅSKE                                                                                                    | Projektarbejde                                   |            |
| 16 (21.4) | Forms & Validation (regex), Vue filter (search), Component Comms, State Management (VUEX) (Localstorage) | Projektarbejde                                   | KJCL       |
| 17 (28.4) | Backend test environment (Strapi, Node server, ?wordpress.com)                                           | Projektarbejde                                   | KJCL       |
| 18 (05.5) | API, JSON, Rapport krav                                                                                  | Projektarbejde                                   | KJCL       |
| 19 (12.5) | PWA, Manifest, Service worker                                                                            | Projektarbejde                                   | KJCL       |
| 20 (19.5) | Recap                                                                                                    | Projektarbejde, forbered spørgsmål               | KJCL       |

## Formalia

- Faget er et lokalt fagelement på 10 ects, det deles mellem KJCL og NISI
- 5 Lektioner pr. uge/dag (8 lekt. pr. ects)
- Eksamen uge 24 - 14/15/16.06.2022

## Projekt krav

Teknologier:

- SASS/SCSS (brug partials)
- Semantic HTML (ARIA, accessible rich internet application)
- Vue CLI, Vue.js
- Single Page App
- Routing (vue router)
- Git Repo
- PWA (optional)
- Testing

Funktionalitet:

- Read JSON data (API) to generate page content
- Dynamic Routing / Source-Detail Page
- Filtering (e.g. in search)
- Form + Validation
- Responsive Website
- Deployment to a static Web Server

(Det er ikke tilladt at bruge et CSS framework ?)

## Rapport krav

[https://www.ucl.dk/laeringsobjekter/rapportskrivning](https://www.ucl.dk/laeringsobjekter/rapportskrivning)

- Forside
- Rapportens forside skal der specificeres følgende: titel, antal tegn, dato for aflevering,institutionens navn, de enkeltes studerendes navne (udelad CPR), produktets webadresse (URL),Gitlab adresse, samt login informationer til adgang og brug af produktet (brugernavn, password osv.).
- Abstract (skal være engelsk)
- Projektet skal indeholde et kort abstract. Abstract er ikke det samme som konklusionen. Abstract skal indeholde en kortfattet beskrivelse af, hvad projektets emne er, de vigtigste spørgsmål der er dækket, udforskede områder og konklusioner.
- Problemformulering
- Projektet skal indeholde en kort, klar og veldefineret problemformulering, der beskriver projektets overordnede problemstillinger og de sonderende spørgsmål, som projektet forsøger at besvare.
- Værktøjs afsnit, Rapporten skal indeholde et værktøjsafsnit, hvor der beskrives værktøjer, teknologier og værktøjsmodeller anvendt i projektet.
- Analyse/Udvikling
- Forklar dine valg, teknisk arkitektur og udviklingsprocessen.
- Diskussion, I denne del af projektet skal du diskutere dine fund / resultater.
- Konklusion, Det er her du opsummerer hele projektets resultater, så de er helt klare og tydelig for læseren. Her skal du også besvare problemformuleringen.
- Reflektion, På baggrund af jeres konklusion reflektérer i over, hvad har du lært, hvad ville du gøre anderledes, fremtidige planer osv.

## Materiale til studerende

- Opgave til at danne teams [[https://www.ucl.dk/laeringsobjekter/fragruppetilteam](https://www.ucl.dk/laeringsobjekter/fragruppetilteam)](<[https://www.ucl.dk/laeringsobjekter/fragruppetilteam](https://www.ucl.dk/laeringsobjekter/fragruppetilteam)>)
- Vue V3 guide [https://v3.vuejs.org/guide/introduction.html](https://v3.vuejs.org/guide/introduction.html)
- Vue style guide [https://v3.vuejs.org/style-guide/#multi-word-component-names-essential](https://v3.vuejs.org/style-guide/#multi-word-component-names-essential)
- Vue CLI [https://cli.vuejs.org/](https://cli.vuejs.org/)
- Vue devtools [https://devtools.vuejs.org/](https://devtools.vuejs.org/)
- Vue vuex [https://next.vuex.vuejs.org/](https://next.vuex.vuejs.org/)
- Sassdoc [http://sassdoc.com/](http://sassdoc.com/) Sass documentation tool
- Frontendmentor.io [https://www.frontendmentor.io/](https://www.frontendmentor.io/) Frontend challenges and ideas

## Studie ordning

### Indhold

Formålet med faget er at udvikle kompetencer til at skabe webapplikationer ved hjælp af moderne frontend teknologier, arbejdsgange og værktøjer. Det beskriver og diskuterer den nuværende praksis med frontend engineering.

### Læringsmål

**Viden**

Efter afslutning af kurset skal de studerende have erhvervet viden om:

- Client-side webapplikationsarkitektur (DB, frontend, API/Backend)
- Style processing og naming conventions (SCSS, LESS, Stylus, BEM)
- Frontend eco system (module bundling, techstack (ie. Mern), frameworks, typescript) Find god grafik
- Frontend workflows (webpack, gulp, javascript)
- Frontend frameworks (VUE, Andre FE frameworks)
- Kvalitetssikring (Testing pyramid, Dokumentation, GIT, branching strategier, code review, Jest. CI/CD? se Bernie slides ) Gitlab eller Github ?

**Færdigheder**

Efter afslutning af kurset skal de studerende have erhvervet færdigheder til:

- Planlægge, udvikle og implementere/idriftsætte client-side web applikationer baseret på konkrete udviklingsønsker (skal hostes live/deploy, se krav til projekt)
- Vælge egnede værktøjer og metoder til implementering af client-side web applikationer (Framework, bundler, preprocessor. Valg skal begrundes)
- Implementere cross-platform web brugergrænseflader (Responsive, PWA (KJCL))
- Implemetere client-side logik (Routing eller state manager)
- Anvende metoder til kvalitetssikring af client-side web applikationer (se test i videns del)

**Kompetencer**

Efter afslutning af kurset skal de studerende have erhvervet kompetencer til at:

- Analysere komplekse udviklingsønsker til at vælge og anvende egnede workflows, metoder, værktøjer og frameworks til implementering af cross-platform, performant, vedligeholdelsesvenlig (navngivning, dokumentation) og dynamiske client-side web applikationer (modsat af statisk, API)

### Prøveform

**Krav til produkt for prøven**

De studerende udarbejder i grupper en webapplikation og en rapport på minimum 10 og maksimum 15 normalsider for en gruppe.  
Gruppen må højest være på 4 personer.  
Der kan i særlige tilfælde dispenseres, så en gruppe kan bestå af en person.

For at kunne vurdere de studerendes individuelle præstation skal det fremgå af indholdsfortegnelsen for rapporten hvilke afsnit, der er udarbejdet af den enkelte studerende.

Webapplikation og kode, som er udviklet af de studerende, skal være tilgængeligt online for censor og eksamintaor.

**Prøveform**

Prøven er individuel og mundtlig baseret på en skriftlig rapport og en udarbejdet webapplikation.  
Der afsættes 30 minutter pr. eksaminand fordelt på 10 minutters præsentation, 15 minutters eksamination og 5 minutters votering.

**Bedømmelse**

Der gives èn individuel karakter efter 7-trinsskalaen ud fra en helhedsvurdering af den afleverede rapport, web applikation, samt den mundtlige præstation.

Som følge af engelsksproget undervisning i det lokale fagelement, gennemføres prøven på engelsk.

Prøven er med ekstern bedømmelse.

**Forudsætning for deltagelse i prøven**

Som forudsætning for detagelse i prøven, skal den studeernede deltage i en planlagt aktivitet i faget Modern Frontend Development. Se semesterbeskrivelsen for en nærmere beskrivelse af forudsætningskravet og den praktiske afvikling heraf.  
Hvis forudsætningen ikke opfyldes, er den studerene ikke indstillet til prøven og har samtidig brugt et prøveforsøg.
