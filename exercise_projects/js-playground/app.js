/* 
new project: npm init
create .gitignore to exclude node_modules folder
install nodemon: npm i --save-dev nodemon
run nodemon: npm run nodemon
add nodemon to package.json
*/

console.clear();

const stringData = ['school', 'work', 'ice', 'vue', 'sass', 'frontend'];
const numberData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

//array filter https://www.w3schools.com/jsref/jsref_filter.asp
const filteredString = stringData.filter((item) => {
  if (item == ['work']) return item;
});

const filteredNumber = numberData.filter((item) => {
  return item < 2;
});

//array map https://www.w3schools.com/jsref/jsref_map.asp

const largeNumbers = numberData.map((item) => {
  return item * 100;
});

const concatStrings = stringData.map((item) => {
  return `${item} is kool!`;
});

//array reduce https://www.w3schools.com/jsref/jsref_reduce.asp
totalArray = [];
numArray = [];

const summedNumbers = numberData.reduce((total, item) => {
  totalArray.push(total);
  numArray.push(item);
  return total + item;
});

//https://www.w3schools.com/Jsref/met_console_table.asp
console.table({
  stringData: stringData,
  numberData: numberData,
  filteredString: filteredString,
  filteredNumber: filteredNumber,
  largeNumbers: largeNumbers,
  concatStrings: concatStrings,
  totalArray: totalArray,
  numArray: numArray,
  summedNumbers: summedNumbers,
});
