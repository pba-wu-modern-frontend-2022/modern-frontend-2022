# Projektplan

# Team 2

## Deltagere

Line Sørine

Henriette

Malene

Mikkel

## Applikationsidéer

### Plan A

En applikation for madentusiaster som henter opskrifter via API. Opskrifterne formateres til et let læseligt format. Opskrifterne skal være brugervenlige og visuelt og nemme at gå til. Brugerne skal kunne søge, filtrere og gemme de hentede opskrifter samt oprette og dele deres egne.

### Plan B

En applikation for børnefamilier hvor de kan organisere deres tid og indkøb. Et samlet sted hvor de kan lave madplan og oprette indkøbssedler samt organisere pligter og to-do lister.

## Projektstyringsmetode

Scrumbaseret projektstyring via kanban board i github.

Boardet opdeles i følgende lister:

- ‘Blocked’ - Til opgaver der kræver ekstern hjælp
- ‘Backlog’ - Alle opgaver vi ved skal laves
- ‘Sprint backlog’ - Til hvert teammøde tilføjes opgaver til denne liste for at være en del af denne uges sprint
- ‘In Progress’ - Når man går igang med en opgave flyttes den til denne liste.
- ‘Review’ - Når en opgave er udført flyttes den til denne list, for at blive vurderet inden den flyttes til ‘Done’ eller tilbage til ‘Backlog’.
- ‘Done - Week x’ - Der oprettes en liste for hver uge for at holde overblik over hvilke opgaver der er lavet hvornår og hvor meget vi har arbejdet i de enkelte uger.

### Git Branching Strategi

Vi opsætter vores git efter “production branch” strategien. Dette gøres for at holde projektet overskueligt så vi kan hjælpe hinanden. Det er vigtigt at vi er obs på hvilke dele af app'en vi hver især arbejder på for at mindske merge konflikter.

## Risikovurdering

1. Risiko: Vi sidder fast i arbejdsopgaverne. Løsning: Vi sætter opgaven på pause indtil den nødvendige hjælp er i hus (fra underviser eller medstuderende).
2. Risiko: En eller flere i teamet bliver ramt af sygdom. Løsning: Vi kan arbejde online via. discord.
3. Risiko: git eksploderer. Løsning: [https://ohshitgit.com/](https://ohshitgit.com/)
4. Risiko: Vi finder ikke et API der passer til vores projekt. Løsning: Vi gemmer data i vores egen database og henter dem via API.
5. Risiko: Vi skal lave noget vi ikke har lært i undervisningen endnu. Løsning: Vi snakker med underviseren om at rykke emnet frem, så vi kan nå det i forhold til vores tidsplan.
6. Risiko: En eller flere teammedlemmer lever ikke op til teamets aftaler og forventninger. Løsning: Vi forsøger at tage denne type problemer op inden de eskalerer. Først tager man en snak med teammedlemmet og senere, hvis nødvendigt, kan underviser inddrages.
7. Risiko: Undervisningen aflyses så vi mangler viden om et emne. Løsning: Vi kontakter underviser og beder om video materiale til selvstudie.

## Team kontrakt

### Organisering

Primær kommunikation foregår gennem Discord

SCRUM på GitHub

Filhåndtering på Google Drive

Google Kalender til planlægning af møder

### Styrker og svagheder

**Malene**

Styrker: Disciplineret og praktisk-orienteret (Organisator). Opsat på omhyggelighed (Afslutter) og undersøger alle aspekter (Analysator).

Svagheder: Bekymrer sig ofte unødvendigt om mindre detaljer (Afslutter), og har svært ved at give opgaver videre hvis for mange. Kan virke skeptisk over for ændringer (Organisator) og sommetider virke bestemmende (Analysator).

**Mikkel**

Styrker: Kreativ problemløser (idémager). Diplomatisk (formidler).

Svagheder: Opfattes ofte som kritisk og skeptisk (analysator). Har svært ved at afslutte opgaver. Distræt (idémager).

**Line**

Styrker: Omhyggelig og perfektionist (Afslutter) Meget koncentreret om mine mål og opgaver (specialist) Nysgerrig og undersøger muligheder (kontaktskaberen)

Svagheder: Perfektionistisk og bekymring sig ofte om detaljer (afslutter) Har svært ved at give mine opgaver videre (specialist) Kan nemt miste interessen ved områder jeg synes er svære (kontaktskaberen).

**Henriette**

Styrker: Er omhyggeligt og perfektionistisk(Afslutteren), vedholdenden og diciplineret (Organisator), udadvent og god til at få folk med på ideér (kontaktskaberen)

Svagheder: Bekymre mig om for mange detaljer og kan have svært ved at give slip (afslutteren), skal bruge lidt tid til at acceptere forandringer(organisatoren), kan godt miste interessen når noget ikke er nyt og spændende mere(kontaktskaberen)

### Forventninger til arbejdsindsats

Hvis man sidder fast skal man søge hjælp hos de andre i teamet, men man har ansvar for de opgaver ens navn står på.

Der forventes at vi mødes fysisk til undervisning i Modern Frontend Development og arbejder på projektet medmindre andet er aftalt.

## Tidsplan med milestones

Februar

<table>
  <tr>
   <td>Dato
   </td>
   <td>24/2 
   </td>
   <td>28/2
   </td>
  </tr>
  <tr>
   <td>Tidspunkt
   </td>
   <td>kl.12:15 - 15
   </td>
   <td>10? - 15
   </td>
  </tr>
  <tr>
   <td>Milestone
   </td>
   <td>Projektplan og plan godkendt 
   </td>
   <td>
   </td>
  </tr>
</table>

Marts

<table>
  <tr>
   <td>Dato
   </td>
   <td>1/3
   </td>
   <td>2/3
   </td>
   <td>3/3
   </td>
   <td>4/3
   </td>
   <td>10/3
   </td>
   <td>17/3
   </td>
   <td>24/3
   </td>
   <td>31/3
   </td>
  </tr>
  <tr>
   <td>Tidspunkt
   </td>
   <td>8-15
   </td>
   <td>8-15
   </td>
   <td>8-15
   </td>
   <td>8-15
   </td>
   <td>kl.12:15
   </td>
   <td>kl.8-11:30
   </td>
   <td>12:15-15
   </td>
   <td>12:15
   </td>
  </tr>
  <tr>
   <td>Milestone
   </td>
   <td>
   </td>
   <td>Prototype klar til programmering
   </td>
   <td>
   </td>
   <td>Beslutte hvilket API der skal bruges og hente det
   </td>
   <td>Database klar til produktion
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>

April

<table>
  <tr>
   <td>Dato
   </td>
   <td>7/4
   </td>
   <td>21/4
   </td>
   <td>28/4
   </td>
  </tr>
  <tr>
   <td>Tidspunkt
   </td>
   <td>8-15
   </td>
   <td>12:15-15
   </td>
   <td>12:15-15
   </td>
  </tr>
  <tr>
   <td>Milestone
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>

Maj

<table>
  <tr>
   <td>Dato
   </td>
   <td>5/5
   </td>
   <td>12/5
   </td>
   <td>19/5
   </td>
   <td>25/5
   </td>
  </tr>
  <tr>
   <td>Tidspunkt
   </td>
   <td>12:15-15
   </td>
   <td>12:15-15
   </td>
   <td>12:15-15
   </td>
   <td>Kl.??
   </td>
  </tr>
  <tr>
   <td>Milestone
   </td>
   <td>
   </td>
   <td>Rapport klar til InDesign 
   </td>
   <td>
   </td>
   <td>AFLEVERING
   </td>
  </tr>
</table>

## Beskrivelse af projektet og ideerne

### Plan A

En applikation for madentusiaster som henter opskrifter via API. Opskrifterne formateres til et let læseligt format. Opskrifterne skal være brugervenlige og visuelt og nemme at gå til. Brugerne skal kunne søge, filtrere og gemme de hentede opskrifter samt oprette og dele deres egne.

#### Hvem er kunden? Hvem kan have nytte af projektet?

Som udgangspunkt er applikationen et selvstændigt produkt, men man kunne også forestille sig en forretningsmodel hvor man solgte app’en til de enkelte madbloggere.

#### Hvem er målgruppen?

Målgruppen er madentusiaster. Folk der gerne vil gemme deres opskrifter digitalt, kunne dele dem med andre og finde nye opskrifter.

## Funktionalitet

- **Brugeren**
  - Login
  - Opret bruger
  - Gemme sine egne opskrifter
  - Privat/offentlig opskrift
  - Opskrift arkiv - opret flere “kogebøger”
  - Madplan (drag and drop)
  - Holde styr på madvarer i køleskabet
  - Oprette indkøbsseddel
  - Dele opskrifter med andre brugere
  - Profilside
- **Sider**
  - Forside med søgefelt og enkelte forslag
  - Inspirationsside
  - Dashboard/feed når logget ind
- **Søge opskrifter**
  - Filtrering af søgeresultater - ingredienser, tid, sværhedsgrad, genre/køkken, måltid, vegansk?, allergier, sæson, popularitet, udstyr, kalorier, “Oprettet af mig”
  - Evt. dropdown ved siden af søgefeltet - Oprette af mig / alle opskrifter / professionelle
- **Opskriftssiden**
  - Oplæsning af opskrifter
  - Dele opskrifter med andre brugere
  - Forfatter
  - Kommentering / billeddeling
  - Like/Yum/Nam/Mums
  - Billede
  - Udstyr
  - Ingrediensliste
    - Bedre omregning af måleenheder
    - Alternative ingredienser
    - Portioner
    - Visualisering af ingredienser
  - Fremgangsmåde
    - Opdelt i steps som kan vinges af og minimeres
    - Tooltip når ingredienser nævnes så man kan se mængde
    - Minut-ur
- Digitalisering af fysiske opskrifter (scan med mobil)
- Finde billigste indkøbssted
- “Se madplan med denne opskrift”
- Frysning/holdbarhed

**Funktionelle krav**

_“Applikationen skal hente og parse data i JSON format fra en API. API data er applikationens indhold. Det vil være en fordel at bruge et CMS som backend der leverer API data.”_

- Dette krav vil blive opfyldt ved at hente opskrifter via et API

_“Applikationen skal implementer dynamisk routing med en eller flere overbliks sider der dynamisk linker til undersider. Dette kaldes også Source-Detail Page.”_

- Dette krav vil blive opfyldt da man efter sin søgning skal kunne klikke sig ind på en opskrift/underside for at læse den

_“Filtrering skal implementeres som f.eks en søgefunktion på siden.”_

- Der implementeres en søgefunktion på opskrifter med filtreringsmuligheder
- Den viste data filtreres i forhold til hvilken bruger der er logget ind og hvem de følger

_“Applikationen skal have en eller flere input forms og der skal som minimum implementeres validering in front-end, men gerne suppleres med back-end validering.”_

- Form validering vil blive implementeret når man opretter sine egne opskrifter samt når man opretter sin profil

_“Applikationen skal bygges responsivt, mobile first, men fungere cross platform på relevante platforme.”_

- Appen laves som en PWA og designes mobile first

_“Applikationen skal udgives så den er offentligt tilgængelig for brugere. Applikationen skal køre fra aflevering af rapport til 1 uge efter alle eksamensforsøg.”_

- Dette krav opfyldes ved at udgive appen med eksempelvis Microsoft Azure

## Plan B

En applikation for børnefamilier hvor de kan organisere deres tid og indkøb. Et samlet sted hvor de kan lave madplan og oprette indkøbssedler samt organisere pligter og to-do lister.

#### Hvem er kunden? Hvem kan have nytte af projektet?

Applikationen er et selvstændigt produkt.

#### Hvem er målgruppen?

Børnefamilier der ønsker at bruge en digital løsning til at organiser pligter,tid og indkøb mm.

**Funktionelle krav**

_“Applikationen skal hente og parse data i JSON format fra en API. API data er applikationens indhold. Det vil være en fordel at bruge et CMS som backend der leverer API data.”_

- Dette krav vil blive opfyldt ved at adskille frontend og backend med et API-lag.

_“Applikationen skal implementer dynamisk routing med en eller flere overbliks sider der dynamisk linker til undersider. Dette kaldes også Source-Detail Page.”_

- Dette krav vil blive opfyldt da

_“Filtrering skal implementeres som f.eks en søgefunktion på siden.”_

- Filtrering i kalender/to-do liste.

_“Applikationen skal have en eller flere input forms og der skal som minimum implementeres validering in front-end, men gerne suppleres med back-end validering.”_

- Form validering vil blive implementeret når man f.eks. opretter en begivenhed i kalenderen.

_“Applikationen skal bygges responsivt, mobile first, men fungere cross platform på relevante platforme.”_

- Appen laves som en PWA og designes mobile first

_“Applikationen skal udgives så den er offentligt tilgængelig for brugere. Applikationen skal køre fra aflevering af rapport til 1 uge efter alle eksamensforsøg.”_

- Dette krav opfyldes ved at udgive appen med eksempelvis Microsoft Azure

## Vedrørende begge planer

**Tekniske krav**

En single page application der henter data via et API. Backend og frontend skal være adskilt af et API-lag.

Frontend laves i vue.js og Vue CLI. Dynamisk routing opsættes ved hjælp af vue router.

Vi anvender semantiske HTML tags for at gøre koden lettere at læse for mennesker såvel som robotter.

Applikationen skal styles med SASS/SCSS og brug af partials. Vi følger best practice naming conventions og bruger bindestreg i vores CSS-klasser. Vi vil lave vores styling så generel som muligt så den samme kode bruges flest mulige gange.

Projektet bliver udviklet med versionsstyring på git for at sikre kvaliteten.

Implementering af software test i applikationen er noget vi mangler viden om.

**Gennem undervisning og arbejdet med projektet opnås der viden om:**

- Client-side webapplikationsarkitektur
- Style processing og naming conventions
- Frontend eco system
- Frontend workflows
- Frontend frameworks
- Kvalitetssikring

**Vi forventer at projektet udvikler vores færdigheder og kompetencer til:**

- Planlægning, udvikling og implementering/idriftsætning client-side web applikationer baseret på konkrete udviklingsønsker (skal hostes live/deploy, se krav til projekt)
- Valg af egnede værktøjer og metoder til implementering af client-side web applikationer
- Implementering cross-platform web brugergrænseflader
- Implementering client-side logik
- Anvendelse af metoder til kvalitetssikring af client-side web applikationer
- Analysere komplekse udviklingsønsker til at vælge og anvende egnede workflows, metoder, værktøjer og frameworks til implementering af cross-platform, performant, vedligeholdelsesvenlig og dynamiske client-side web applikationer

## Kommentarer nisi

- overvej om ikke alle jeres filer skal ligge på git istedet for at involvere google drive ?
- Meget fin risikovurdering, realistiske risici
- Superfine overvejelser, hvilken ide har i valgt ?
- Har github ikke en milestone funktionalitet i deres kanban ?
- Flotte reflektioner ifht. læringsmål

## Kommentarer kjcl

- Jeg kender ikke denne branching strategi I nævner, mener I environment branching?
  - https://www.creativebloq.com/web-design/choose-right-git-branching-strategy-121518344
    - https://docs.microsoft.com/en-us/azure/devops/repos/git/git-branching-guidance?view=azure-devops
      - https://www.gitkraken.com/learn/git/best-practices/git-branch-strategy
      - Jf. risikohåndtering: Kan I bruge daily standups til at håndtere blokeringer
      - Fedt med kravliste (funktionalitet). Når I opretter stories i jeres SCRUM board, prøv at tidsestimere og hvis opgaven ikke kan tidsestimeres så skal den splittes op
