Projektplan/problemstilling
https://pba-wu-modern-frontend-2022.gitlab.io/modern-frontend-2022/#/article/6

Instruktioner:

1.  Lav en åben brainstorm hvor hele teamet bidrager til hvad projektet og applikationen skal indeholde. Alle ideer er gyldige uanset hvor vilde de er!

- Nyhedssite.
- gamescore site.
- fanside til et spil.
- En hjemmeside med life hacks.
- En hjemmeside om hunde?
- en H.C Andersen hjemmeside.
- En hjemmeside der viser den danske kongerække.
- En hjemmeside der giver et nemt overblik over hvem der er i folketinget.
- webshop.
- Valutaomregner.
- Aktie og eller krypto kurser.
- Live calculator der udregner hvor mange mennesker er døde eller syge af corona på verdensplan.
- En hjemmeside der hjælper folk med hvad for en bog de skal læse. (Man kan evt. lave nogle spørgsmål og så vælger hjemmesiden en bog ud fra svarene).

2.  Dan jer et overblik over hvor meget tid i har til rådighed til at bygge applikationen, i kan regne med at hver eftermiddag samt hele uge 9.

- 4 timer om torsdagen * 4 *3 = 48 timer + 2 yderligere undervisningsgange = 56 timer. Der vil højstsandsynligt forekomme dage hvor vi arbejder med individuelle opgaver omkring projektet. så vi rammer nok 70+ timer sammenlagt

3.  Fra jeres brainstorm finder i de 2 bedste og mest realistiske/brugbare ideer. Hvem er kunden, hvem er målgruppen, hvem kan have nytte af projektet osv.

    - Første ide: **"En hjemmeside der giver et nemt overblik over hver der er i folketinget."**

      - **"Hvem er kunden?"** Kan være nyhedsfirmaer eller politiske partier som vil have informationer tilgængelige for vælgere.
      - **"Hvem er målgruppen?"** politisk orienterede. Alder og køn kan være varieret (svært at præcisere uden at lave en undersøgelse på hvem der er mest politisk anlagt).
      - **"Hvem kan have nytte af projektet?"** Personer som vil være mere politisk orienteret, eller som leder efter informationer på politkere, hvad de står for, hvilket parti de er i osv.
      - **"Hvilke problematikker kan vi se ved at gå videre med denne ide?"** ~~Hvis vi ikke kan gøre brug af dummy data. så vil vi skulle bruge meget tid på at finde informationer selv om de forskellige politikere der er i Danmark.~~

    - Anden ide: **"Bogholderi"**
      - **"Hvem er kunden?"** Odense Bibliotek
      - **"Hvem er målgruppen?"** alle Aldre som søger bøger, dog skal de kunne betjene en app
      - **"Hvem kan have nytte af projektet?"** Bogorme som vil have lettere ved at finde bøger som kan interessere dem.
      - **"Hvilke problematikker kan vi se ved at gå videre med denne ide?"** umiddelbart ingen problemer.

4.  Find en projektstyrings metode i vil anvende, jeg vil anbefale noget scrum agtigt https://www.scrum.org/resources/what-is-scrum Det vigtige er at i aftaler nogle faste møder hvor i opdaterer hinanden på projektets status og fordeler opgaver så alle har noget at arbejde med hele tiden. Det er også nødvendigt at aftale hvordan i vil samarbejde om kode med GIT. Her kan i for eksempel tage udgangs punkt i en branching strategi https://docs.gitlab.com/ee/topics/gitlab_flow.html

- Vi gør brug af SCRUM da vi alle har prøvet det før og ved hvordan det skal bruges.
- Ift. Git vil vi gøre brug af branches, vi har alle prøvet det før, men begrænset for nogle af os så vi vil se hvordan det kommer til at gå.

5.  Lav en risiko vurdering der indeholder en beskrivelse af 10 ting der kan gå galt i projektet og hvad i vil gøre hvis hver enkelt ting går galt.

- Vi mangler informationer som ikke er kommet frem i undervisningen.
  - Find oplysningerne på egen hånd.
- En eller flere er syge
  - Holde hinanden i hånden, vi skriver på nuværende tid sammen på FB og Discord. Hvilket virker helt fint, da dem der er syge stadigvæk har været med i det vi laver.
- Tekniske problemer, en computer virker ikke.
  - Hjælp hinanden eller spørg IT på skolen.
- Diktatur (tidligere erfaringer)
  - Alle er nødt til at komme med deres meninger, selvom nogle stemmer er højere end andre så er der flertal for andre, så vores kommunikation skal være iorden.
- Generelle uoverensstemmelser som F.eks arbejdsmængde fordeling, personer der ikke kan sammen, nogle kommer ikke til undervisning
  - Kommunikation skal være KEY. Vi snakker allerede godt sammen, og det virker som om vi er enige om hvad vi skal og hvad vi hver især kan.
- Mac brugere.....
  - ingen hjælp at hente. Stemt ud af øen. "take off your apron and leave hells kitchen"
- Anger Issues (CODE DOESNT **\*\*\*** WORK)
  - Hjælp hinanden eller ihvertfald så vi kan suffer sammen
- Prioriteringer
  - Når vi skal arbejde så arbejder vi sammen, så arbejde eller kæresten må vente til vi har fri. Hvis ikke det er muligt så må arbejde tages med hjem så det er klar til næste gang vi ses.

6.  Lav en team kontrakt hvor i beskriver jeres belbin roller inkl. styrker og svagheder, hvordan i kan kontakte hinanden, hvilken arbejds indsats i kan forvente af hinanden osv. Formålet er at i får afstemt forventninger til hinanden og hvilke krav der er realistiske at forvente af jeres team medlemmer.

- Vi har lavet et docs med vores afstemning af os selv og hinanden.
  https://docs.google.com/document/d/1kHAAMs-j-87SbjqUyiNvMHlKq_B5NpOjdnl1wUzAaa8/edit?usp=sharing

7. Lav en tidsplan med milestones for projektet, det skal bare være de store blokke i projektet, ikke mange detaljer.

- API virker
- Sidens design er fornuftig
- Sidens funktionalitet virker
- Filtrering virker

8. Udfyld en projektplan som indeholder:

- Team navn
- Deltagere
- En kort beskrivelse af ideerne til applikationen, beskriv begge 2, den ene er plan a og den anden plan b.
- Valgt projektstyrings metode
- Risikovurdering
- Team kontrakt
- Tidsplan med milestones
- En mere grundig beskrivelse af projektet og ideerne. Den skal indeholde en beskrivelse af hvordan applikationen kan leve op til de tekniske og funktionelle krav, samt hvordan projektet lever op til fagets læringsmål. Her vil vi gerne have at i besvarer hvert enkelt punkt i hhv. tekniske og funktionelle krav samt hvert enkelt læringsmål.

9. Læg projektplanen i jeres github/gitlab projekt som en .md fil og link til filen i team dokumentet https://docs.google.com/document/d/1M2JEnv71nqK5GroJmJvUCmHT1RfGJXCLmoJRaydzTIQ/edit?usp=sharing

# Projektplan opsummering

- Gruppe 6
- Mikkel Bang, Nikolai Raahauge, Peter Eilerskov, Casper Hauge
- Tjek punkt NR.3
- SCRUM
- Den er ikke høj som vi ser den, vores kommunikation og overensstemmelse med projektet er ens for os. Så vi tror alle på at vi når i mål.
- Er lavet i et docs så står i punkt 6
- Tjek punkt NR. 7
-

# Spørgsmål

- Hvis vi laver en webshop, kan man så få kurven til at virke som SPA? Eller siden i det hele taget. Da der vil være mange funktioner så er nødvendigt at have en database til?
  - Kan man godt gøre uden at det bryder SPA. da det er "skelettet" forbliver det samme.
- Er det stadig SPA hvis man har et API kald? (Siden må jo tilspørge API'et om data, medmindre man slet ikke tæller API med under SPA)
  - ja selvom vi tilgår data fra API så forbliver vores side stadigvæk SPA. da det er "skelettet" på vores side stadig er det samme som vi har loaded det første gang.
-

# Noter

- Tjek API informationer
- Dagene der er sat af til projekt i MFD er efter pausen kl 12:15 og indtil 16 tiden.
- Med det vi laver så tjek efter læringsmålene og om vi kommer til at opfylde dem med det vi har lavet/ det vi laver.
-

## kommentarer nisi

- flot brainstorm
- har i glemt uge 9 i jeres tidsregnskab ?
- I skal være skarpe på alle krav og mål, snak med gr. 2 eller 3 om hvordan de har gjort
- forklar lige kort ideen
- jeres spørgsmål ?

## kommentarer kjcl

- Ideerne er for fluffy, er det hvilke politkere der er i bygningen og hvad er sammenhængen. Er det hvilke partier der er i bygningen. Hvad mener I? Bogholderi har samme udfordring
  - Ydermere er bogholderi altså en del af en regnskabsafdeling i en virksomhed
- Inspiration til branching strategier
  - https://www.creativebloq.com/web-design/choose-right-git-branching-strategy-121518344
  - https://docs.microsoft.com/en-us/azure/devops/repos/git/git-branching-guidance?view=azure-devops
  - https://www.gitkraken.com/learn/git/best-practices/git-branch-strategy
- Få tidsestimater og nedbrydning af jeres milestones så I bedre kan håndtere dem
