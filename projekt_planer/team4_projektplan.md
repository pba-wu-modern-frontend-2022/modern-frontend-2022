**Team navn**

Team 4

**Deltagere**

Mark Schwartz Jørgensen, Benjamin Fabricius Porsgaard, Laurits Søndergaard

**En kort beskrivelse af ideerne til applikationen beskriv begge 2, den ene er plan a og den anden plan b**

Plan A er Webshop til spil, webshoppen ville være noget lignende G2A eller Kinguin

Plan B er trustpilot til spil, hvor brugere kan lave ratings på de forskellige spil

**Valgt projektstyringsmetode**

Dette bliver SCRUM da vi har mest erfaring med det

**Risikovurdering**

[https://docs.google.com/document/d/1ckfaC6jqXeZpAf3BYTTDKASw2hmC0T2r/edit?usp=sharing&amp;ouid=105828667867594295301&amp;rtpof=true&amp;sd=true](https://docs.google.com/document/d/1ckfaC6jqXeZpAf3BYTTDKASw2hmC0T2r/edit?usp=sharing&ouid=105828667867594295301&rtpof=true&sd=true)

**Team kontrakt**

[https://docs.google.com/document/d/1ZoTYme7xa4wb9OA4m1K_A9F27uHI0AmlNzuhXns3F2E/edit?usp=sharing](https://docs.google.com/document/d/1ZoTYme7xa4wb9OA4m1K_A9F27uHI0AmlNzuhXns3F2E/edit?usp=sharing)

**Tidsplan med milestones**

Estimat på 50 timer, arbejdes der hver torsdag eftermiddag frem til den 25 maj hvor der skal afleveres projekt.
Fra start til slut er der sat nogle delmål og estimat på hvor lang tid der skal bruges på disse delmål.
Research - 1 uge
Framework, Wireframe Prototype XD (adobe XD) &amp; Testing - 2 uger
Frontend webudvikling - 4 uger
Backend, Data, CMS &amp; API - 2 uger
Tilpasning af framework &amp; backend - uger
Q/A tid - NA
Rapport - 12 uger

**En mere grundig beskrivelse af projektet og ideerne. Den skal indeholde en beskrivelse af hvordan applikationen kan leve på til de tekniske og funktionelle krav, samt hvordan projektet lever op til fagets læringsmål. Her vil vi gerne have at i besvarer hvert enkelt punkt i hhv. tekniske og funktionelle krav samt hvert enkelt læringsmål**

I dette projekt er gruppen kommet frem til 2 forskellige ideer men går ud fra tanken at arbejde med plan a, hvilket er webshop til spil hvor der vil arbejdes ud fra ideen til G2A og kinguin. Udover dette vil der arbejdes med anmeldelser, hvor der vil blive etableret anmeldelser direkte på spillets side så folk får muligheden for at se hvor godt det er med det samme frem for at de skal søge sig frem til det.
Dette føler vi er en mangel på andre sider f.eks G2A, gamere vil gerne have muligheden for at se anmeldelser direkte inden de bestiller spillet.
Der vil blive arbejdet med Vue framework inden for dette projekt, hvor der vil blive stylet med sass/scss.
Derudover er der tanker om at have api kørende ved hjælp af diverse anmeldelser for at trække dataen ind på siden på den måde. Da det er en webshop vi er ude i, vil der også bliver undersøgt muligheden for lagerbeholdning af diverse spil osv.
Der bliver arbejdet med tanken om en PWA

Ud over alt dette, vil der blive researchet en del på hvordan sådan en hjemmeside skal fungere og hvordan man vil kunne få dette ud og blive testet ved de rigtige bruger. Inden webapplikationen bliver sendt live vil der blive arbejdet i at teste softwaren, for at undgå diverse fejl og mangler som kan blive et problem.

I forhold til projektet bliver der arbejdet med Git, for at sikre versions styring.

_ **Teknologi krav:** _

- Styling skrives i SASS/SCSS med brug af partials.

- Semantic HTML skal anvendes (se [ARIA, accessible rich internet application](https://webaim.org/techniques/aria/))

- Vue3.js og Vue CLI skal anvendes

- Applikationen skal være en Single Page Application (SPA)

- Routing skal implementeres med vue router

- Applikationen skal kvalitetssikres i et offentligt tilgængeligt repository (public github/gitlab etc. projekt)

- Applikationens kode skal versions styres ved brug af GIT

- Applikationen skal implementere software test

- Applikationen må meget gerne implementeres som en PWA men det er valgfrit

_ **Funktionalitets krav:** _

- Applikationen skal hente og parse data i JSON format fra en API. API data er applikationens indhold. Det vil være en fordel at bruge et CMS som backend der leverer API data.

- Applikationen skal implementer dynamisk routing med en eller flere overbliks sider der dynamisk linker til undersider. Dette kaldes også Source-Detail Page.

- Filtrering skal implementeres som f.eks en søgefunktion på siden.

- Applikationen skal have en eller flere input forms og der skal som minimum implementeres validering in front-end, men gerne suppleres med back-end validering.

- Applikationen skal bygges responsivt, mobile first, men fungere cross platform på relevante platforme.

- Applikationen skal udgives så den er offentligt tilgængelig for brugere. Applikationen skal køre fra aflevering af rapport til 1 uge efter alle eksamensforsøg.

## kommentarer nisi

- Flot risiko vudering
- Fedt i har valgt noget i selv kan li!
- 50 timer? Har i ikke glemt uge 9
- I mangler at besvare funtionalitets og teknologi krav ?
- Opfyldelse af læringsmål er ikke super skarpe, snak med gr. 2 eller 3 om hvordan de har gjort

## kommentarer kjcl

- De 2 ideer er meget åbne og kan risikere at drukne i feature creep hvis I ikke er meget fokuseret på stories
- Har I nogen overvejelser om hvilken software der skal levere API, altså hvilken backend
