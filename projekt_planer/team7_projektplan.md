# Velkommen til Team 7

**Deltagere:**

- David
- Sven-Erik
- Mathias
- Lone

Kort beskrivelse af applikationerne (idéer)

**Applikation: Køleskabet**<br>
Masser af opskrifter og ideer generet ud fra de forskellige indregienser brugeren har til rådighed i deres køleskab.
Der kan udvides med nice-to-have-features såsom indkøbsseddel, lommeregnere og andre perks.

**Applikation: Astronomi (Space Events)**<br>
Visualisering af informationer af forskellige elementer fra rummet, såsom planeter og deres placering, og menneskeskabte elementer (f.eks. satellitter).
Kan få anbefalinger om hvornår og hvor der er de bedste "views" til stjerner, stjerneskud, og planeter. Der kan indtastes lokation hvis dette vil være af interesse at opleve.

# Projektet

Noter gemmes i Gitlab projektets tilhørende Wiki<br>
https://gitlab.com/MDF_gr7/mdf_project/-/wikis/home

## Værktøjer

Der gøres brug af følgende værktøjer og metoder

- BEM (Block, Element, Modifier)
- Git
- Gitlab
- Vue
- Sass

## Branching strategy<br>

GitHub Flow
Der findes en main branch, hvor der løbende oprettes feature branches der senere bliver merget til main branchen når det er færdigt.<br>
Læs mere her:<br>
https://docs.gitlab.com/ee/topics/gitlab_flow.html

## Projektstyring

Der anvendes SCRUM

Der aftales at holde et gruppemøde mindst én gang om ugen.
Der opsættes milestone med en deadline som er vigtig at overholde. Deadlines er pt. den første fredag i hver måned. (To be changed)

Opgaver og milestones indsættes på en tavle i GitLab.<br>Det kan findes i Gitlab projektet ved at vælge **Issues** > **Boards**
<br>
https://gitlab.com/MDF_gr7/mdf_project/-/boards

# Applikationen: Brainstorm, features

**Køleskabs SPA**

- Ingredienser (API, fødevare informationer)
- Forslag til mad og retter, baseret på de ingredienser brugeren indtaster
  - Kan udvides med "Indkøbsseddel" feature på manglende varer
- Bottomless scrolling
- Vejledninger til opskrifter, og brug af madvare.
- Indkøbsseddel (mobil app til netto, rema etcs)
  - Tilbud fra flere forskellige butikker (API)
  - Kan være baseret på den ret man ønsker at lave (manglende varer)?
  - Kan udvides med prisudregner (manglende varer)
- Rabat kuponer (API)
- Health checkup (foodplaner funktionalitet)

**Astronomi (Space Events)**

- Placering af måne, planeter, osv (API)
- Kombinere med vejrudsigt (API! Er det godt vejr til at se stjerner samt et perfekt tidspunkt?)
- Space Events (Eclipse, Blodmåne, etc, API)
- Man-made Space Events (Hvornår nye ting bliver sent up i rummet, API)
- Stjernebillede interaktion (Måske svær)
- Track et event eller kategori (F.eks. få notifikation få dage før en eclipse starter)

# Gruppekontrakt

_Link til kontrakt_

# Tidsplan og milestones

Uge 9 er projekt uge

Den 4. marts (MILESTONE 1)

Den 1. april (MILESTONE 2)

Den 6. maj (MILESTONE 3)

_Deadline den 25. maj_

# Risiko

Liste over ting som kan gå galt.

- [Sygdom](#sygdom)
- [Overholdelse af møder og milestones](#overholdelse-af-m%C3%B8der-og-milestones)
- [Merging conflicts](#merging-conflicts)
- [Mangel på API eller data til planlagte funktioner](#mangel-p%C3%A5-api-eller-data-til-planlagte-funktioner)
- [Projektet bliver for stort, tungt, og lang loading tid](#projektet-bliver-for-stort-tungt-og-lang-loading-tid)
- [Mangel på erfaring](#mangel-p%C3%A5-erfaring)
- [Projektet er for splittet](#projektet-er-for-splittet-mange-forskellige-features)
- [Problemer med hosting](#problemer-med-hosting)

## Sygdom

Vi regner med, at der er en meget høj sandsynlighed for, at en eller flere i gruppen rammes a sygdom. Det er derfor vigtigt at vi finder en løsning på dette.

**Tilbagemelding**<br>
Kommer personen til gruppemøderne og gruppearbejde?<br>
Kan arbejdsopgavernes deadline overholdes af den enkelte person som rammes af sygdom?<br>
Der skal tages en vurding løbende om tidsplanen kan overholdes, og hvad der kan gøres for at tilpasse tidsplanen.

**Der arbejdes på opgaverne, selv når sygdom opstår**<br>
Gruppen forventer, at den enkelte person stadig forsøger at færdiggøre arbejde, uanset om personen er syg eller ej. Der kan opstå alvorlig sygdom, hvor gruppemedlemmet straks må tilbagemelde dette for at ændre i planen.

## Overholdelse af møder og milestones

Vi regner med at der overholdes møder og for det meste milestones.

**Møder**<br>
Vi regner stærkt med at møder bliver holdt uanset om der opstår sygdom eller andre forhindringer for nogle af gruppemedlemmerne.<br>
Hvis mødet alligevel ikke bliver holdt på det planlagte tidspunkt, planlægges et nyt tidspunkt så det ugentlige møde stadig bliver afholdt.

**Milestones**<br>
Der kan komme uforudsete forhindringer for at overholde deadlines for Milestones. Her er der vigtigt at vi hele tiden holder os opdateret på vores forskellige opgaver og om vi er på rette vej til at nå det enkelte milestone.

Hvis det ikke overholdes, må der diskuteres i gruppen om projektet skal skæres ned på nogle af de forskellige planlagte features.

## Merging Conflicts

Vi forventer at vi støder ind i flere merging conflicts undervejs.

Først vurderes størrelsen på konflikten. Er konflikten mindre, løses det af personen som forsøger at merge.

Et større merge conflict kontaktes den person som har skreveet den del af koden, hvor konflikten opstår.

## Mangel på API eller data til planlagte funktioner

Da API er en vigtig del af nogle af de forskellige idéer til features til applikationen, er det vigtigt at undersøge tidligt i projektet, om den enkelte feature kan realiseres.

Der tages en beslutning tidligt i projektplanlægningen om featuren er realistisk på nuværende tidspunkt.

Hvis den enkelte feature ikke kan realiseres, må det skrottes og der bliver fokuseret på et andet.

## Projektet bliver for stort, tungt, og lang loading tid

Undgå at bruge nye frameworks til enkelte eller mindre opgaver.
Undersøg hvad der kan gøres for at minimere loading.

## Mangel på erfaring

Læser op i fritiden, og gør brug af vejledning.

Der kan også kontaktes lærerne hvis der er spørgsmål

nisi@ucl.dk<br>
ioto@ucl.dk<br>
kjcl@ucl.dk

## Projektet er for splittet (mange forskellige features)

Vi har flere features som lyder spændende og sjove, men tvivler på, at der bliver tid til at lave alle i en kvalitet som vi er tilfredse med.
Her er det valgt, at vi laver en favoritliste af vores features, og vælger at arbejde med de features, vi bedst kan lide og mener at vi kan nå i en god kvalitet.

## Problemer med hosting

Køb et domæne.

# Deadline

Aflevering af rapport og applikation er den 25. maj 2022. <br>
Hvert milestone skal overholdes i den første fredag i hvert måned (to be changed).

# Læringsmål vs Projektet

Hvordan lever projektet op til læringskravene?

### Client-side webapplikationsarkitektur

- API calls
- Bottomless scrolling

### Style processing og naming conventions

- Vi bruger værktøjerne BEM og Sass

### Frontend eco system

- Dont know (pls help)

### Frontend workflows

- Processen hvordan applikationen bygges

### Frontend frameworks

- Der bliver anvendt Vue!
- Der kan muligvis udvides, f.eks. ved kuponer (webscrabing?)

### Kvalitetssikring

- Se [Risikoanalysen](#risiko)

Der er fokus på at arbejde med de nyeste teknologier inden for frontend udvikling:

Frontend/Javascript framework (Vue), SASS/SCSS, Semantic HTML, SPA, PWA, GIT og testing.

## kommentarer nisi

- Forklar kort ideen
- Hvad indeholder jeres milestones
- Jeres spørgsmål

## kommentarer kjcl

- Pas på med at skrive osv. i app beskrivelserne, det er svært at estimere tid på
- Prøv at estimere tid på jeres milestone og sørg for at splitte dem op til stories når i arbejder med dem
