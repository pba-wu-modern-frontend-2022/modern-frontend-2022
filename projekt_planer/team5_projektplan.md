# Projekt plan for gruppe 5

### Navne: Nicole Buys, Edna Dasalla Valter, Amalie Lauritsen og Katharina Appel.

<br>

## Ideer til applikation:

<br>

> ### 1. CMS webshopbuilder til bands
>
> En hjemmeside bygger med webshop løsning til bands der mangler en hurtig hjemmeside, vi tænker forskellige temaer, men med mulighed for at lægge logo og andet ind selv

> ### 2. Fake band hjemmeide med webshop
>
> Webshop med filtrering

## Projektstyringsmetode

Vi har valgt bruge scrum, med projektstyring i gitlab.
Amalie er scrummaster og i samarbejde med Nicole bliver planerne lagt.

## Link til risiko vurdering

https://gitlab.com/t7353/MFD-Project/-/blob/main/risikovurdering.md

# Team kontrakt

> ## HVORDAN ER VI ORGANISERET, OG HVAD ER VORES ANSVAR?
>
> (Ex. Hvilket medie gør vi brug af tildeling og organisering af dokumenter / filer)
> <br>
> Gitlab (til versionstyring, scrum og mødekalender), Google Drev og Facebook messenger chat.
> Vi bruger klassen discord til online møder.
> Overhold aftaler.
> <br> > <br>
> Aftale dokument:
> https://docs.google.com/document/d/12HnW2jvsmEcoU4YEQDCkMH5VPVGVibfPPBAZBnrG0IM/edit > <br> > <br>
> Links dokument:
> https://docs.google.com/document/d/12l4g8DZjZ3URtTxYwc-9FPIKhnvmlN3BUTBZzK4qBng/edit > <br> > <br>
> MFD research:
> https://docs.google.com/document/d/1s_YmTD8ja5UROUNdx27d0N5ZrGhr03v3s-9_9Ieq1QE/edit?usp=sharing

<br>
<br>
<br>

> ## Hvad er vi gode til?
>
> > ### Katharina Appel <br>
> >
> > Opstarter, kontaktskaber, koordinator <br> >> **Personlige kompetencer:** <br>
> > Lærer hurtigt, kan se videoer på x2. Nysgerrig, videbegærlig.
> > Jeg elsker projekt arbejde. Hyper fokusering. God til at bruge google. <br> >>**Faglige:** <br>
> > html, css, sass, .net, php, sql, mssql, normalisering, js, videns søgning.
>
> <br>
> <br>

> > ### Nicole
> >
> > Idémager – analysator – Specialist <br> >> **Personlige kompetencer:** <br>
> > Lærer hurtigt, adaptiv, kreativ <br> >>**Faglige kompetencer** <br>
> > html, css, sass, javascript, php, SQL, database design, Wordpress
>
> <br>
> <br>

> > ### Amalie Lauritsen
> >
> > Afslutter, organisator, specialist <br> >> **Personlige kompetencer:** <br>
> > Dybdegående, kan godt lide at nørde og sætte mig helt ind i tingene, organiseret og struktureret <br> > > **Faglige kompetencer:** <br>
> > HTML, CSS, JavaScript, SASS, WordPress,
>
> <br>
> <br>

> > ### Edna Valter
> >
> > Idemager - Organisator - Afsultter <br> >> **Personlige Kompetencer** <br>
> > Initiativ, fleksibel, ambitiøs, kreativ, selvstændig, godt humør <br> >> **Faglige Kompetencer** <br>
> > html, css, js, sass, database, SQL, MySQL, Asp.net

<br>
<br>

> ## Hvad kan vi ikke?
>
> ### Katharina Appel <br>
>
> - Hvis jeg har en dårlig dag, kan det være svært for mig at finde overskuddet.
> - Hvis jeg ikke er hyper fokuseret, kan jeg have svære ved at koncentrere mig
> - Bliver hurtigt påvirket af en stemning i lokalet
>
> ### Nicole Buys
>
> - kan være flyvsk og blive overvældet.
> - prokrastination ofte startet af perfektionisme.
>   Nogle gange skal jeg bare have at vide at det ikke skal være perfekt.
>
> ### Amalie Lauritsen
>
> - B-menneske - arbejder bedst (med egne og selvstændige opgaver) om eftermiddagen, aftenen og natten
> - Kan have det rigtig svært med gruppearbejde
>   Lærer langsomt
> - Skal helst motiveres af andre for at have motivation selv
>
> ### Edna Valter
>
> - dårlig til at skrive rapport
> - svært ved at udtrykke ordene om, hvad jeg mener
>   mine danske ord er begrænsede
> - jeg føler utilpas til at tale foran publikum.

<br>
<br>

> ## Hvor og hvornår arbejder vi
>
> På discord eller på skolen, mødes kl. 9 medmindre andet aftales løbende. <br>
> Møder bliver bekræftede i aftaledokumentet <br>
> Gitlab mødekalender

<br>
<br>

> ## Samarbejde og kommunikation
>
> Hvis nogen er i tvivl om noget, hjælper vi hinanden. <br>
> Skal deltage i alle opgaver medmindre andet aftales løbende. <br>
> Overhold aftaler. <br>
> Hvis man sidder fast; <br>
> mens vi arbejder sammen fysisk skal man spørge om hjælp efter 10 min.<br>
> selvstændigt efter en time.
>
> Hvis nogle står får en del selvstændigt, er det deres ansvar at forklarer/dokumentere til andre.
> Logbog bruges til dette;
> https://docs.google.com/document/d/1k7CYHLHIeg6ZzQ78jfayDN4hHpPVC9h6zGdA981FJ_8/edit#

<br>
<br>

> ## Projektleder
>
> Nicole Buys

<br>
<br>

> ## Hvad er projektlederens ansvar?
>
> Holde overblikket for projektet og vores mål. <br>
> Sørge for at vi opfylder krav. <br>
> Skære i gennem, når der bliver kørt cirkler. <br>
> Kontakt til vejleder, sørge for at vi tænker på hvornår vi kan bruge vejledning.

<br>
<br>

> ## Hvem er scrumaster?
>
> Amalie

<br>
<br>

> ## Hvad er gruppens ansvar over for projektleder?
>
> Overholde aftaler og deadlines. <br>
> Sætte grænser og sige til hvis der er noget man syntes er galt. <br>
> Sige til hvis opgaven er for stor eller for lille <br>
> Hvis der er relationelle problemer, går vi til projektleder <br>

<br>
<br>

> ## Praktisk
>
> Vi mødes på discord kl. 9 som udgangspunkt er der mødepligt med mindre andet er aftalt. Kommunikation foregår på Facebook Messenger. <br>
> Pomodoro method bliver implementeret i dage hvor vi mødes online for at arbejde selvstændig. <br>
> Deling af dokumenter og filer foregår på Google Docs og Drev. <br>
> Hver dag er der et møde. <br > Konflikter snakker vi om

<br>
<br>

> ## Ved regelbrud
>
> Første gang: Vi snakker <br>
> Anden gang: Advarsel <br>
> Tredje gang: Vi tager fat i en underviser <br>
>
> Hvis det fortsætter kan udvisning komme på tale.

<br>
<br>

> # Tidsplan med milepæle
>
> **Milepæle points, de bliver ikke anvendt som vandfaldsmetode**
> Organiseret i denne [google kalender](https://calendar.google.com/calendar/u/0?cid=MnZzbG1wZmZoMXBkbXVqbjI5bmZmaXZwMmtAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ)
>
> 1.  Begyndende problemformulering
> 2.  Research og tek research
> 3.  Tek valg
> 4.  Prototyping/udvikling
> 5.  Test
> 6.  Deployment
> 7.  Rapport kladde
> 8.  Rapport færdig

## kommentarer nisi

- Flot risikovurdering
- Er det nødvendigt med både gitlab og google drev til filer ? Det hele på gitlab
- I skal være skarpe på alle krav og mål, de mangler i jeres plan, snak med gr. 2 eller 3 om hvordan de har gjort. Sikre at ide kan opfylde mål og krav!
- Milepæle kan komme på gitlab

## kommentarer kjcl

- Milepæle må gerne have en form for estimat, evt. brydes ned hvis estimater ikke kan vurderes med relativ sikkerhed
- De 2 ideer er meget åbne og kan risikere at drukne i feature creep hvis I ikke er meget fokuseret på stories
